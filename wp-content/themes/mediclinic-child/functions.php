<?php

/*** Child Theme Function  ***/

if (!function_exists('mediclinic_mikado_child_theme_enqueue_scripts')) {
    function mediclinic_mikado_child_theme_enqueue_scripts()
    {

        $parent_style = 'mediclinic-mikado-default-style';

        wp_enqueue_style('mediclinic-mikado-child-style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));
    }

    add_action('wp_enqueue_scripts', 'mediclinic_mikado_child_theme_enqueue_scripts');
}

/**
 * test
 */
class Partner_Widget extends WP_Widget
{

    function __construct()
    {

        parent::__construct(
            'partner_widget',  // Base ID
            'Partner Widget'   // Name
        );

        add_action('widgets_init', function () {
            register_widget('Partner_Widget');
        });

    }

    public $args = array(
        /*'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',*/
        'before_widget' => '<div class="widget-wrap">',
        'after_widget' => '</div></div>'
    );

    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        ?>
        <div style='display: flex; flex-wrap: wrap; justify-content: center'>

            <div>
                <?= wp_get_attachment_image((9148), 'thumbnail', false); ?>
            </div>

            <div>
                <?= wp_get_attachment_image((9147), 'thumbnail', false); ?>
            </div>
            <div>
                <?= wp_get_attachment_image((9149), 'thumbnail', false); ?>
            </div>
        </div>
        <div style='display: flex; flex-wrap: wrap; justify-content: center'>

            <div>
                <?= wp_get_attachment_image((9148), 'thumbnail', false); ?>
            </div>
            <div>
                <?= wp_get_attachment_image((9149), 'thumbnail', false); ?>
            </div>
        </div>
        <div style='display: flex; flex-wrap: wrap; justify-content: center'>

            <div>
                <?= wp_get_attachment_image((9148), 'thumbnail', false); ?>
            </div>

            <div>
                <?= wp_get_attachment_image((9147), 'thumbnail', false); ?>
            </div>
            <div>
                <?= wp_get_attachment_image((9149), 'thumbnail', false); ?>
            </div>
        </div>
        <?php
        echo $args['after_widget'];

    }


}

$Partner_Widget = new Partner_Widget();

/**
 * Autoriser les fichiers SVG
 */
/** Get role of current user to check if he is administrator */
$user_meta = get_userdata(get_current_user_id());
$user_roles = $user_meta->roles;

/** Check if current user is administrator to allow the svg files */
if (in_array('administrator',$user_roles)){

    function wpc_mime_types($mimes) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }

    add_filter('upload_mimes', 'wpc_mime_types');
}
