<div id="mkdf-testimonials<?php echo esc_attr($current_id) ?>" class="mkdf-testimonial-content">

	<div class="mkdf-testimonial-content-inner">
		<div class="mkdf-testimonial-text-holder">
			<div class="mkdf-testimonial-text-inner mkdf-grid">
				<h5 class="mkdf-testimonial-text"><?php echo trim($text) ?></h5>
                <div class="mkdf-separator-quote fa fa-quote-right"></div>
				<?php if (!empty($author)) { ?>
					<div class = "mkdf-testimonial-author">
						<h5 class="mkdf-testimonial-author-text"><?php echo esc_attr($author)?>
						</h5>
					</div>
				<?php } ?>
				<?php if (!empty($title)) { ?>
					<h6 class="mkdf-testimonial-job-title">
						<?php echo esc_html($title); ?>
					</h6>
				<?php } ?>
			</div>

		</div>
	</div>	
</div>
