<div id="mkdf-testimonials<?php echo esc_attr($current_id) ?>" class="mkdf-testimonial-content">
    <div class="mkdf-testimonial-content-inner">
        <?php if (has_post_thumbnail($current_id)) { ?>
            <div class="mkdf-testimonial-image-holder">
                <?php esc_html(the_post_thumbnail($current_id)) ?>
            </div>
        <?php } ?>
        <p class="mkdf-testimonial-text">
            <?php echo trim($text) ?>
        </p>
        <span class="mkdf-testimonial-separator"></span>
        <?php if (!empty($author)) { ?>
            <div class="mkdf-testimonial-author">
                <h5 class="mkdf-testimonial-author-text">
                    <span>
                        <?php echo esc_attr($author)?>
                    </span>
                </h5>
            </div>
        <?php } ?>
        <?php if(!empty($title)){ ?>
            <h6 class="mkdf-testimonial-job-title">
                <?php echo esc_attr($title) ?>
            </h6>
        <?php }?>
    </div>
</div>
