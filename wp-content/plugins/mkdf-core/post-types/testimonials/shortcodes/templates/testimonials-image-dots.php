<div class="mkdf-tes-image-nav">
    <?php if ($query_results->have_posts()):
        while ($query_results->have_posts()) : $query_results->the_post(); ?>
        <div class="mkdf-tes-image-single">
                <span class="mkdf-tes-image-holder">
                    <?php echo get_the_post_thumbnail(get_the_ID()) ?>
                </span>
            </div>
        <?php endwhile;
        else: ?>
            <span><?php esc_html_e( 'Sorry, no posts matched your criteria', 'mediclinic' ); ?></span>
    <?php endif;
    wp_reset_postdata(); ?>
</div>