<?php

if(!function_exists('mkdf_core_map_team_single_meta')) {
    function mkdf_core_map_team_single_meta() {

        $meta_box = mediclinic_mikado_create_meta_box(array(
            'scope' => 'team-member',
            'title' => esc_html__('Doctor Info', 'mkdf-core'),
            'name'  => 'team_meta'
        ));

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_position',
            'type'        => 'text',
            'label'       => esc_html__('Specialty', 'mkdf-core'),
            'description' => esc_html__('The doctor\'s role within the team', 'mkdf-core'),
            'parent'      => $meta_box
        ));

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_icon',
            'type'        => 'image',
            'label'       => esc_html__('Member Icon', 'mkdf-core'),
            'description' => esc_html__('The doctor\'s icon', 'mkdf-core'),
            'parent'      => $meta_box
        ));

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_email',
            'type'        => 'text',
            'label'       => esc_html__('Email', 'mkdf-core'),
            'description' => esc_html__('The doctor\'s email', 'mkdf-core'),
            'parent'      => $meta_box
        ));

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_phone',
            'type'        => 'text',
            'label'       => esc_html__('Phone', 'mkdf-core'),
            'description' => esc_html__('The doctor\'s phone', 'mkdf-core'),
            'parent'      => $meta_box
        ));



        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_education',
            'type'        => 'text',
            'label'       => esc_html__('Degrees', 'mkdf-core'),
            'description' => esc_html__('The doctor\'s education', 'mkdf-core'),
            'parent'      => $meta_box
        ));

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_training',
            'type'        => 'text',
            'label'       => esc_html__('Training', 'mkdf-core'),
            'description' => esc_html__('The doctor\'s training', 'mkdf-core'),
            'parent'      => $meta_box
        ));

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_short_bio',
            'type'        => 'text',
            'label'       => esc_html__('Short Bio', 'mkdf-core'),
            'description' => esc_html__('The doctor\'s short bio', 'mkdf-core'),
            'parent'      => $meta_box
        ));

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_bio',
            'type'        => 'textarea',
            'label'       => esc_html__('Bio', 'mkdf-core'),
            'description' => esc_html__('The doctor\'s full bio', 'mkdf-core'),
            'parent'      => $meta_box
        ));

        mediclinic_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_doctor_workdays',
                'type' => 'checkboxgroup',
                'label' => esc_html__('Work Days', 'mkdf-core'),
                'description' => esc_html__('Choose days of the week when this doctor is available', 'mkdf-core'),
                'parent' => $meta_box,
                'options' => array(
                    'monday' => esc_html__('Monday', 'mkdf-core'),
                    'tuesday' => esc_html__('Tuesday', 'mkdf-core'),
                    'wednesday' => esc_html__('Wednesday', 'mkdf-core'),
                    'thursday' => esc_html__('Thursday', 'mkdf-core'),
                    'friday' => esc_html__('Friday', 'mkdf-core'),
                    'saturday' => esc_html__('Saturday', 'mkdf-core'),
                    'sunday' => esc_html__('Sunday', 'mkdf-core')
                ),
                'args' => array(
                    'enable_empty_checkbox' => false,
                    'inline_checkbox_class' => true
                )
            )
        );

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_start_time',
            'type'        => 'text',
            'default_value'     => '9',
            'label'       => esc_html__('Working hours from: ', 'mkdf-core'),
            'description' => esc_html__('Set starting time from which users can start booking services ( for example if you want 8:00, set value 8)', 'mkdf-core'),
            'parent'      => $meta_box
        ));

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_end_time',
            'type'        => 'text',
            'default_value'     => '18',
            'label'       => esc_html__('Working hours to: ', 'mkdf-core'),
            'description' => esc_html__('Set end time to which users can book doctor(for example if you want 20:00, set value 20)', 'mkdf-core'),
            'parent'      => $meta_box
        ));

        mediclinic_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_team_member_booking_period',
            'type'        => 'text',
            'default_value'        => '30',
            'label'       => esc_html__('Booking periods', 'mkdf-core'),
            'description' => esc_html__('Set booking periods in minutes(for example 30)', 'mkdf-core'),
            'parent'      => $meta_box
        ));


        for($x = 1; $x < 6; $x++) {

            $social_icon_group = mediclinic_mikado_add_admin_group(array(
                'name'   => 'mkdf_team_member_social_icon_group'.$x,
                'title'  => esc_html__('Social Link ', 'mkdf-core').$x,
                'parent' => $meta_box
            ));

                $social_row1 = mediclinic_mikado_add_admin_row(array(
                    'name'   => 'mkdf_team_member_social_icon_row1'.$x,
                    'parent' => $social_icon_group
                ));

                    MediclinicMikadoIconCollections::get_instance()->getSocialIconsMetaBoxOrOption(array(
                        'label' => esc_html__('Icon ', 'mkdf-core').$x,
                        'parent' => $social_row1,
                        'name' => 'mkdf_team_member_social_icon_pack_'.$x,
                        'defaul_icon_pack' => '',
                        'type' => 'meta-box',
                        'field_type' => 'simple'
                    ));

                $social_row2 = mediclinic_mikado_add_admin_row(array(
                    'name'   => 'mkdf_team_member_social_icon_row2'.$x,
                    'parent' => $social_icon_group
                ));

                    mediclinic_mikado_create_meta_box_field(array(
                        'type'            => 'textsimple',
                        'label'           => esc_html__('Link', 'mkdf-core'),
                        'name'            => 'mkdf_team_member_social_icon_'.$x.'_link',
                        'hidden_property' => 'mkdf_team_member_social_icon_pack_'.$x,
                        'hidden_value'    => '',
                        'parent'          => $social_row2
                    ));
	
			        mediclinic_mikado_create_meta_box_field(array(
				        'type'          => 'selectsimple',
				        'label'         => esc_html__('Target', 'mkdf-core'),
				        'name'          => 'mkdf_team_member_social_icon_'.$x.'_target',
				        'options'       => mediclinic_mikado_get_link_target_array(),
				        'hidden_property' => 'mkdf_team_member_social_icon_'.$x.'_link',
				        'hidden_value'    => '',
				        'parent'          => $social_row2
			        ));
        }
    }

    add_action('mediclinic_mikado_meta_boxes_map', 'mkdf_core_map_team_single_meta', 46);
}