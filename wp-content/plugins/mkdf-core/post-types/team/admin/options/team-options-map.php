<?php

if ( ! function_exists('mediclinic_mikado_team_options_map') ) {
    function mediclinic_mikado_team_options_map() {

        mediclinic_mikado_add_admin_page(array(
            'slug'  => '_team',
            'title' => esc_html__('Team', 'mkdf-core'),
            'icon'  => 'fa fa-camera-retro'
        ));

        $panel_booking = mediclinic_mikado_add_admin_panel(array(
            'title' => esc_html__('Booking Form', 'mkdf-core'),
            'name'  => 'panel_booking_form',
            'page'  => '_team'
        ));

        mediclinic_mikado_add_admin_field(array(
            'name'        => 'doctor_booking_email',
            'type'        => 'text',
            'label'       => esc_html__('Email for Booking Requests', 'mkdf-core'),
            'description' => esc_html__('Enter the email to which booking requests will be sent. If left empty, they will be sent to the admin.', 'mkdf-core'),
            'parent'      => $panel_booking,
            'args'        => array(
                'col_width' => 3
            )
        ));
    }

    add_action( 'mediclinic_mikado_options_map', 'mediclinic_mikado_team_options_map', 14);
}