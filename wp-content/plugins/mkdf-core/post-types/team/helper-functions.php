<?php

if(!function_exists('mkdf_core_team_meta_box_functions')) {
	function mkdf_core_team_meta_box_functions($post_types) {
		$post_types[] = 'team-member';
		
		return $post_types;
	}
	
	add_filter('mediclinic_mikado_meta_box_post_types_save', 'mkdf_core_team_meta_box_functions');
	add_filter('mediclinic_mikado_meta_box_post_types_remove', 'mkdf_core_team_meta_box_functions');
}

if(!function_exists('mkdf_core_team_scope_meta_box_functions')) {
	function mkdf_core_team_scope_meta_box_functions($post_types) {
		$post_types[] = 'team-member';
		
		return $post_types;
	}
	
	add_filter('mediclinic_mikado_set_scope_for_meta_boxes', 'mkdf_core_team_scope_meta_box_functions');
}

if(!function_exists('mkdf_core_team_enqueue_meta_box_styles')) {
	function mkdf_core_team_enqueue_meta_box_styles() {
		global $post;
		
		if($post->post_type == 'team-member'){
			wp_enqueue_style('mkdf-jquery-ui', get_template_directory_uri().'/framework/admin/assets/css/jquery-ui/jquery-ui.css');
		}
	}
	
	add_action('mediclinic_mikado_enqueue_meta_box_styles', 'mkdf_core_team_enqueue_meta_box_styles');
}

if(!function_exists('mkdf_core_register_team_cpt')) {
	function mkdf_core_register_team_cpt($cpt_class_name) {
		$cpt_class = array(
			'MikadoCore\CPT\Team\TeamRegister'
		);
		
		$cpt_class_name = array_merge($cpt_class_name, $cpt_class);
		
		return $cpt_class_name;
	}
	
	add_filter('mkdf_core_filter_register_custom_post_types', 'mkdf_core_register_team_cpt');
}

if(!function_exists('mkdf_core_get_single_team')) {
	/**
	 * Loads holder template for doctor single
	 */
	function mkdf_core_get_single_team() {
		$team_member_id = get_the_ID();
		
		$params = array(
			'sidebar_layout' => mediclinic_mikado_sidebar_layout(),
			'position'       => get_post_meta($team_member_id, 'mkdf_team_member_position', true),
			'email'          => get_post_meta($team_member_id, 'mkdf_team_member_email', true),
			'phone'          => get_post_meta($team_member_id, 'mkdf_team_member_phone', true),
			'address'        => get_post_meta($team_member_id, 'mkdf_team_member_address', true),
			'education'      => get_post_meta($team_member_id, 'mkdf_team_member_education', true),
			'training'     	 => get_post_meta($team_member_id, 'mkdf_team_member_training', true),
			'short_bio'      => get_post_meta($team_member_id, 'mkdf_team_member_short_bio', true),
			'bio'      		 => get_post_meta($team_member_id, 'mkdf_team_member_bio', true),
			'workdays'       => get_post_meta($team_member_id, 'mkdf_doctor_workdays', true),
			'start_time'     => get_post_meta($team_member_id, 'mkdf_team_member_start_time', true),
			'end_time'       => get_post_meta($team_member_id, 'mkdf_team_member_end_time', true),
			'booking_period' => get_post_meta($team_member_id, 'mkdf_team_member_booking_period', true),
			'social_icons'   => mkdf_core_single_team_social_icons($team_member_id),
			'bookworkdays'  =>  mkdf_core_single_team_weekdays($team_member_id)
		);
		
		mkdf_core_get_cpt_single_module_template_part('templates/single/holder', 'team', '', $params);
	}
}

if(!function_exists('mkdf_core_single_team_weekdays')){
	function mkdf_core_single_team_weekdays($id) {

		$weekdays = get_post_meta($id, 'mkdf_doctor_workdays', true);
		$disable_week_days =array();

		if (!in_array("sunday", $weekdays)) {
			$disable_week_days[] = '0';
		}
		if (!in_array("monday", $weekdays)) {
			$disable_week_days[] = '1';
		}
		if (!in_array("tuesday", $weekdays)) {
			$disable_week_days[] = '2';
		}
		if (!in_array("wednesday", $weekdays)) {
			$disable_week_days[] = '3';
		}
		if (!in_array("thursday", $weekdays)) {
			$disable_week_days[] = '4';
		}
		if (!in_array("friday", $weekdays)) {
			$disable_week_days[] = '5';
		}
		if (!in_array("saturday", $weekdays)) {
			$disable_week_days[] = '6';
		}


		$disabled_week_days = implode(", ", $disable_week_days);

		return $disabled_week_days;

	}
}

if(!function_exists('mkdf_core_single_team_social_icons')) {
	function mkdf_core_single_team_social_icons($id){
		$social_icons = array();
		
		for ($i = 1; $i < 6; $i++) {
			$team_icon_pack = get_post_meta($id, 'mkdf_team_member_social_icon_pack_' . $i, true);
			if($team_icon_pack !== '') {
				$team_icon_collection = mediclinic_mikado_icon_collections()->getIconCollection(get_post_meta($id, 'mkdf_team_member_social_icon_pack_' . $i, true));
				$team_social_icon     = get_post_meta($id, 'mkdf_team_member_social_icon_pack_' . $i . '_' . $team_icon_collection->param, true);
				$team_social_link     = get_post_meta($id, 'mkdf_team_member_social_icon_' . $i . '_link', true);
				$team_social_target   = get_post_meta($id, 'mkdf_team_member_social_icon_' . $i . '_target', true);
				
				if ($team_social_icon !== '') {
					$team_icon_params = array();
					$team_icon_params['icon_pack']                  = $team_icon_pack;
					$team_icon_params[$team_icon_collection->param] = $team_social_icon;
					$team_icon_params['link']                       = !empty($team_social_link) ? $team_social_link : '';
					$team_icon_params['target']                     = !empty($team_social_target) ? $team_social_target : '_self';
					
					$social_icons[] = mediclinic_mikado_execute_shortcode('mkdf_icon', $team_icon_params);
				}
			}
		}
		
		return $social_icons;
	}
}

if(!function_exists('mkdf_core_get_team_category_list')) {
	function mkdf_core_get_team_category_list($category = '') {
		$number_of_columns = 3;
		
		$params = array(
			'number_of_columns'   => $number_of_columns
		);
		
		if(!empty($category)) {
			$params['category'] = $category;
		}
		
		$html = mediclinic_mikado_execute_shortcode('mkdf_team_list', $params);
		
		print $html;
	}
}

if ( ! function_exists( 'mkdf_action_send_booking_form' ) ) {

	function mkdf_action_send_booking_form() {

		if ( isset($_POST['data']) ) {

			$error = false;
			$responseMessage = '';

			$email_data = $_POST['data'];
			$nonce = $email_data['nonce'];

			if ( wp_verify_nonce( $nonce, 'mkdf_validate_booking_form' ) ) {

				//Validate

				if ( $email_data['contact'] ) {
					$phone = esc_html($email_data['contact']);
				} else {
					$error = true;
					$responseMessage = esc_html__('Please insert valid phone', 'mkdf-core');
				}

				if ( $email_data['name'] ) {
					$name = esc_html($email_data['name']);
				} else {
					$error = true;
					$responseMessage = esc_html__('Please insert valid name', 'mkdf-core');
				}

				if ( $email_data['time'] ) {
					$time = esc_html($email_data['time']);
				} else {
					$error = true;
					$responseMessage = esc_html__('Please choose a valid time', 'mkdf-core');
				}

				if ( $email_data['date'] ) {
					$date = esc_html($email_data['date']);
				} else {
					$error = true;
					$responseMessage = esc_html__('Please choose a valid date', 'mkdf-core');
				}

				if ( $email_data['doctor'] ) {
					$doctor = esc_html($email_data['doctor']);
				} else {
					$error = true;
					$responseMessage = esc_html__('Please select a doctor', 'mkdf-core');
				}


				if ( $email_data['message'] ) {
					$message = esc_html($email_data['message']);
				}

				//Send Mail and response
				if ( $error ) {

					wp_send_json_error( $responseMessage );

				} else {

					//Get email address
					$mail_to = mediclinic_mikado_options()->getOptionValue('doctor_booking_email');
					if ($mail_to == '') {
						$mail_to = get_option('admin_email');
					}

					$headers = array();

					$messageTemplate = esc_html__('From', 'mkdf-core'). ': ' . $name . "\r\n";
					$messageTemplate .= esc_html__('Phone', 'mkdf-core') . ': ' . $phone . "\r\n\n";
					$messageTemplate .= esc_html__('Doctor', 'mkdf-core') . ': ' . $doctor . "\r\n\n";
					$messageTemplate .= esc_html__('Requested date', 'mkdf-core'). ': ' . $date . "\r\n";
					$messageTemplate .= esc_html__('Requested time', 'mkdf-core') . ': ' . $time . "\r\n\n";
					$messageTemplate .= esc_html__('Message', 'mkdf-core') . ': ' . $message . "\r\n\n";

					$mail_sent = wp_mail(
						$mail_to, //Mail To
						esc_html__('New Booking Request', 'mkdf-core'), //Subject
						$messageTemplate, //Message
						$headers //Additional Headers
					);

					if ($mail_sent) {
						$responseMessage = esc_html__('Booking request sent successfully', 'mkdf-core');
						wp_send_json_success( $responseMessage );
					}
					else {
						$responseMessage = esc_html__('Booking request failed. Please try later.', 'mkdf-core');
						wp_send_json_error( $responseMessage );
					}
				}

			}


		} else {
			$message = esc_html__('Please review your enquiry and send again', 'mkdf-core');
			wp_send_json_error( $message );
		}

	}

	add_action( 'wp_ajax_mkdf_action_send_booking_form', 'mkdf_action_send_booking_form' );
	add_action( 'wp_ajax_nopriv_mkdf_action_send_booking_form', 'mkdf_action_send_booking_form' );

}