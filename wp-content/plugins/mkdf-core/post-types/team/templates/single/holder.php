<div class="mkdf-full-width">
	<div class="mkdf-full-width-inner clearfix">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php if(post_password_required()) {
				echo get_the_password_form();
			} else { ?>
				<div class="mkdf-team-single-holder">

						<div <?php echo mediclinic_mikado_get_content_sidebar_class(); ?>>
							<div class="mkdf-team-single-outer">
								<div class="mkdf-grid">
									<?php mkdf_core_get_cpt_single_module_template_part('templates/single/parts/info', 'team', '', $params); ?>
								</div>
								<?php mkdf_core_get_cpt_single_module_template_part('templates/single/parts/content', 'team', '', $params); ?>
							</div>
						</div>
						<?php if($sidebar_layout !== 'no-sidebar') { ?>
							<div <?php echo mediclinic_mikado_get_sidebar_holder_class(); ?>>
								<?php get_sidebar(); ?>
							</div>
						<?php } ?>
				</div>
			<?php } ?>
		<?php endwhile;	endif; ?>
	</div>
</div>