<div class="mkdf-team-single-info-holder">
	<div class="mkdf-grid-row">
		<div class="mkdf-ts-image-holder mkdf-grid-col-3">
			<?php the_post_thumbnail(); ?>
			<div class="mkdf-doctor-info-holder">
				<h5 itemprop="name" class="mkdf-name entry-title"><?php the_title(); ?></h5>
				<h6 class="mkdf-position"><?php echo esc_html($position); ?>			</h6>
				<div class="mkdf-ts-bio-holder">
					<?php if(!empty($phone)) { ?>
						<div class="mkdf-contact-info">
							<span aria-hidden="true" class="ion-android-call mkdf-ts-bio-icon"></span>
							<span class="mkdf-ts-bio-info"><?php echo esc_html($phone); ?></span>
						</div>
					<?php } ?>
					<?php if(!empty($email)) { ?>
						<div class="mkdf-contact-info">
							<span aria-hidden="true" class="ion-ios-email mkdf-ts-bio-icon"></span>
							<span itemprop="email" class="mkdf-ts-bio-info"><a href="mailto:<?php echo sanitize_email(esc_html($email)); ?>"><?php echo sanitize_email(esc_html($email)); ?></a></span>
						</div>
					<?php } ?>
					<div class="mkdf-contact-info mkdf-book-now">
						<span aria-hidden="true" class="ion-calendar mkdf-ts-bio-icon"></span>
						<span itemprop="email" class="mkdf-ts-bio-info"><?php echo esc_html__('Book appointment', 'mkdf-core'); ?></span>
					</div>

				</div>
				<div class="mkdf-social-holder">
					<?php foreach ($social_icons as $social_icon) {
						echo wp_kses_post($social_icon);
					} ?>
				</div>
			</div>
			<?php mkdf_core_get_cpt_single_module_template_part('templates/single/parts/booking-form', 'team', '', $params); ?>
		</div>
		<div class="mkdf-ts-details-holder mkdf-grid-col-9">
			<div class="mkdf-ts-bio-holder">
				<?php if(!empty($short_bio)) { ?>
					<h4><?php echo esc_html($short_bio);?></h4>
				<?php } ?>
				<?php if(!empty($bio)) { ?>
					<p><?php echo esc_html($bio);?></p>
				<?php } ?>
				<?php if(!empty($position)) { ?>
					<div class="mkdf-ts-info-row">
						<table>
							<tr>
								<td class="mkdf-title-wrapper"><span class="mkdf-ts-bio-info-title"><?php echo esc_html__('Specialty ', 'mkdf-core');?></span></td>
								<td><span class="mkdf-ts-bio-info"><?php echo esc_html($position); ?></span></td>
							</tr>
						</table>
					</div>
				<?php } ?>
				<?php if(!empty($education)) { ?>
					<div class="mkdf-ts-info-row">
						<table>
							<tr>
								<td class="mkdf-title-wrapper"><span class="mkdf-ts-bio-info-title"><?php echo esc_html__('Degrees ', 'mkdf-core');?></span></td>
								<td><span class="mkdf-ts-bio-info"><?php echo esc_html($education); ?></span></td>
							</tr>
						</table>
					</div>
				<?php } ?>
				<?php if(!empty($training)) { ?>
					<div class="mkdf-ts-info-row">
						<table>
							<tr>
								<td class="mkdf-title-wrapper"><span class="mkdf-ts-bio-info-title"><?php echo esc_html__('Training ', 'mkdf-core');?></span></td>
								<td><span class="mkdf-ts-bio-info"><?php echo esc_html($training); ?></span></td>
							</tr>
						</table>
					</div>
				<?php } ?>
                <?php mkdf_core_get_cpt_single_module_template_part('templates/single/parts/doctor-days', 'team', '', $params); ?>
			</div>
		</div>
	</div>
</div>