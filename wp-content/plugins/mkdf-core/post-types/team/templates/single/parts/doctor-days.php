<?php
$doctor_days = get_post_meta(get_the_ID(), 'mkdf_doctor_workdays', true);
if (is_array($doctor_days) && count($doctor_days)) { ?>
    <div class="mkdf-ts-info-row">
        <table>
            <tr>
                <td class="mkdf-title-wrapper"><span class="mkdf-ts-bio-info-title"><?php esc_html_e('Work Days', 'mkdf-core'); ?></span></td>
                <td>
                    <span class="mkdf-ts-bio-info">
                        <?php foreach($doctor_days as $workday) {
                            if($workday !== ''){ ?>
                                <span class="mkdf-workdays-wrapper">
                                    <span class="ion-checkmark-round mkdf-ts-bio-icon"></span>
                                    <span class="mkdf-doctor-workday">  <?php echo esc_html__(ucfirst($workday)); ?> </span>
                                </span>
                        <?php } } ?>
                    </span>
                </td>
            </tr>
        </table>
    </div>
<?php } ?>