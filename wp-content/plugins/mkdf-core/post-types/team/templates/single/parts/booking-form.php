<div class="mkdf-booking-form " data-workdays="<?php echo esc_html__($bookworkdays);?>" data-start-time="<?php echo esc_attr__($start_time);?>" data-end-time="<?php echo esc_attr__($end_time);?>" data-period="<?php echo esc_attr__($booking_period);?>">
    <div class="mkdf-booking-form-inner">
        <span class="mkdf-booking-subtitle"><?php echo esc_html__('We are here for you', 'mkdf-core')?></span>
        <h3 class="mkdf-booking-title"><?php echo esc_html__('Book appointment', 'mkdf-core')?></h3>
        <div class="mkdf-bf-form-fields">
            <form method="POST">
                <div class="mkdf-bf-form-item mkdf-bf-doctor mkdf-bf-narrow">
                    <input type="text" class="mkdf-bf-input-doctor" name="mkdf-booking-doctor" placeholder="<?php esc_attr_e('Doctor', 'mkdf-core'); ?>" value="<?php the_title(); ?>" readonly="readonly">
                </div>

                <div class="mkdf-bf-form-item mkdf-bf-name mkdf-bf-narrow">
                    <input type="text" class="mkdf-bf-input-name" name="mkdf-booking-name" placeholder="<?php esc_attr_e('Your Full Name', 'mkdf-core'); ?>">
                </div>

                <div class="mkdf-bf-form-item mkdf-bf-contact mkdf-bf-narrow">
                    <input type="text" class="mkdf-bf-input-contact" name="mkdf-booking-contact" placeholder="<?php esc_attr_e('Phone', 'mkdf-core'); ?>">
                </div>

                <div class="mkdf-bf-form-item mkdf-date mkdf-bf-narrow">
                    <input type="text" class="mkdf-bf-input-date" name="mkdf-booking-date" placeholder="<?php esc_attr_e('Date', 'mkdf-core'); ?>">
                    <div class="mkdf-bf-field-icon-holder"></div>
                </div>

                <div class="mkdf-bf-form-item mkdf-time mkdf-bf-narrow">
                    <input type="text" class="mkdf-bf-input-time" name="mkdf-booking-time" placeholder="<?php esc_attr_e('Time', 'mkdf-core'); ?>">
                    <div class="mkdf-bf-field-icon-holder"></div>
                </div>


                <span class="nonce"><?php wp_nonce_field('mkdf_validate_booking_form', 'mkdf_nonce_booking_form_'.rand()); ?></span>
                <?php echo '<script type="application/javascript">if (typeof MikadoAjaxUrl === "undefined") {var MikadoAjaxUrl = "'.admin_url('admin-ajax.php').'"; }</script>'; ?>
                <div class="mkdf-bf-form-button">
                    <?php echo mediclinic_mikado_execute_shortcode('mkdf_button', array(
                        'html_type'    => 'input',
                        'input_name'   => 'mkdf-booking-submit',
                        'text'         => esc_attr__('Book appointment', 'mkdf-core'),
                        'size'         => 'small',
                        'custom_attrs' => array(
                            'data-sending-label' => esc_attr__('Sending...', 'mkdf-core')
                        )
                    )); ?>
                </div>
                <div class="mkdf-bf-form-response-holder"></div>

            </form>
        </div>

    </div>
</div>