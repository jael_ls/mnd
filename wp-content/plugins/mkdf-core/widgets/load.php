<?php
if(!function_exists('mediclinic_mikado_load_widget_class')) {
	/**
	 * Loades widget class file.
	 */
	function mediclinic_mikado_load_widget_class(){
		include_once 'widget-class.php';
	}

	add_action('mediclinic_mikado_before_options_map', 'mediclinic_mikado_load_widget_class');
}

if(!function_exists('mediclinic_mikado_load_widgets')) {
	/**
	 * Loades all widgets by going through all folders that are placed directly in widgets folder
	 * and loads load.php file in each. Hooks to mediclinic_mikado_after_options_map action
	 */
	function mediclinic_mikado_load_widgets() {

		foreach(glob(MIKADO_FRAMEWORK_ROOT_DIR.'/modules/widgets/*/load.php') as $widget_load) {
			include_once $widget_load;
		}

		include_once 'widget-loader.php';
	}

	add_action('mediclinic_mikado_before_options_map', 'mediclinic_mikado_load_widgets');
}