<?php

if (!function_exists('mediclinic_mikado_register_widgets')) {
	function mediclinic_mikado_register_widgets() {
		$widgets = apply_filters('mediclinic_mikado_register_widgets', $widgets = array());

		if (mediclinic_mikado_is_woocommerce_installed()) {
			$widgets[]='MediclinicMikadoWoocommerceDropdownCart';

		}

		foreach ($widgets as $widget) {
			register_widget($widget);
		}
	}
	
	add_action('widgets_init', 'mediclinic_mikado_register_widgets');
}