<?php
namespace MikadoCore\CPT\Shortcodes\Process;

use MikadoCore\Lib;
class ProcessHolder implements Lib\ShortcodeInterface {
    private $base;

    public function __construct() {
        $this->base = 'mkdf_process_holder';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name'                    => esc_html__('Mikado Process', 'mkdf-core'),
            'base'                    => $this->getBase(),
            'as_parent'               => array('only' => 'mkdf_process_item'),
            'content_element'         => true,
            'show_settings_on_create' => true,
            'category'                => esc_html__( 'by MIKADO', 'mkdf-core' ),
            'icon'                    => 'icon-wpb-process extended-custom-icon',
            'js_view'                 => 'VcColumnView',
            'params'                  => array(
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'number_of_items',
                    'heading'     => esc_html__('Number of Process Items', 'mkdf-core'),
                    'value'       => array(
                        esc_html__('Three', 'mkdf-core') => 'three',
                        esc_html__('Four', 'mkdf-core')  => 'four',
                        esc_html__('Five', 'mkdf-core')  => 'five'
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => ''
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'animate_process_items',
                    'heading'     => esc_html__('Animate Process Items when they enter the viewport', 'mkdf-core'),
                    'value'       => array(
                        esc_html__('Yes', 'mkdf-core') => 'yes',
                        esc_html__('No', 'mkdf-core')  => 'no',
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => ''
                )
            )
        ));
    }

    public function render($atts, $content = null) {
        $default_atts = array(
            'number_of_items' => '',
            'animate_process_items' => 'yes'
        );

        $params            = shortcode_atts($default_atts, $atts);
        $params['content'] = $content;

        $params['holder_classes'] = array(
            'mkdf-process-holder',
            'clearfix',
            'mkdf-process-holder-items-'.$params['number_of_items'],
            'mkdf-animate-process-items-'.$params['animate_process_items']
        );

        return mkdf_core_get_shortcode_module_template_part('templates/process-holder-template', 'process', '', $params);
    }
}