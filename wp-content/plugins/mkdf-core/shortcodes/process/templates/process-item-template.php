<?php
$icon_html = mediclinic_mikado_icon_collections()->renderIcon($icon, $icon_pack, $params);
?>

<div <?php mediclinic_mikado_class_attribute($item_classes); ?>>
    <div class="mkdf-pi-holder-inner">
        <?php if(!empty($icon)) : ?>
            <div class="mkdf-pi-icon-holder" <?php mediclinic_mikado_inline_style($border_color) ?>>
                <span class="mkdf-pi-icon">
                <?php if(!empty($custom_image)) : ?>
                    <?php echo wp_get_attachment_image($custom_image, 'full'); ?>
                <?php else: ?>
                <?php
                print $icon_html;
                ?>
                <?php endif; ?>
                </span>
            <?php if(!empty($number)) : ?>
                <span class="mkdf-pi-number" <?php mediclinic_mikado_inline_style($number_color) ?>>
                    <?php echo esc_html($number); ?>
                </span>
            <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="mkdf-pi-content-holder">
            <?php if(!empty($title)) : ?>
                <div class="mkdf-pi-title-holder">
                    <h5 class="mkdf-pi-title" <?php mediclinic_mikado_inline_style($title_color) ?>><?php echo esc_html($title); ?></h5>
                </div>
            <?php endif; ?>

            <?php if(!empty($text)) : ?>
                <div class="mkdf-pi-text-holder">
                    <p <?php mediclinic_mikado_inline_style($text_color) ?>><?php echo esc_html($text); ?></p>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>