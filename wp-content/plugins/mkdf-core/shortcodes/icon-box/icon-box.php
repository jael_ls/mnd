<?php
namespace MikadoCore\CPT\Shortcodes\IconBox;

use MikadoCore\Lib;

class IconBox implements Lib\ShortcodeInterface {

    /**
     * @var string
     */
    private $base;

    public function __construct() {
        $this->base = 'mkdf_icon_box';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer. Hooked on vc_before_init
     *
     * @see mkd_core_get_carousel_slider_array_vc()
     */
    public function vcMap() {

        vc_map(array(
            'name'                      => esc_html__('Mikado Icon Box', 'mkdf-core'),
            'base'                      => $this->getBase(),
            'category'                  => esc_html__( 'by MIKADO', 'mkdf-core' ),
            'icon'                      => 'icon-wpb-icon-box extended-custom-icon',
            'allowed_container_element' => 'vc_row',
            'params'                    => array_merge(
                mediclinic_mikado_icon_collections()->getVCParamsArray(),
                array(
                    array(
                        'type'       => 'attach_image',
                        'heading'    => esc_html__('Custom Icon/Image', 'mkdf-core'),
                        'param_name' => 'custom_icon'
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Info Box Type', 'mkdf-core'),
                        'param_name'  => 'box_type',
                        'value'       => array(
                            esc_html__('Standard', 'mkdf-core')  => 'inside-icon',
                            esc_html__('Animated', 'mkdf-core') => 'overlapping-icon'
                        )
                    ),
                    array(
                        'type' 			=> 'dropdown',
                        'heading' 		=> esc_html__('Animate Icon Box', 'mkdf-core'),
                        'param_name' 	=> 'animate_icon_box',
                        'value' 		=> array(
                            esc_html__('No' , 'mkdf-core')  => 'no',
                            esc_html__('Yes' , 'mkdf-core') => 'yes'
                        ),
                        'admin_label' 	=> true,
                        'save_always' 	=> true,
                        'description' 	=> '',
                        'dependency' => array('element' => 'box_type', 'value' => array('inside-icon'))
                    ),
                    array(
                        'type' 			=> 'dropdown',
                        'heading' 		=> esc_html__('Animation Type', 'mkdf-core'),
                        'param_name' 	=> 'animation_type',
                        'value' 		=> array(
                            esc_html__('Entire Box' , 'mkdf-core')  => 'entire-box',
                            esc_html__('Icon Only' , 'mkdf-core') => 'icon-only'
                        ),
                        'admin_label' 	=> true,
                        'save_always' 	=> true,
                        'description' 	=> '',
                        'dependency' => array('element' => 'animate_icon_box', 'value' => array('yes'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Title', 'mkdf-core'),
                        'param_name'  => 'title',
                        'admin_label' => true,
                        'description' => ''
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Title Tag', 'mkdf-core'),
                        'param_name'  => 'title_tag',
                        'value'       => array(
                            ''   => '',
                            'h2' => 'h2',
                            'h3' => 'h3',
                            'h4' => 'h4',
                            'h5' => 'h5',
                            'h6' => 'h6',
                        ),
                        'description' => ''
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => esc_html__('Title Color', 'mkdf-core'),
                        'param_name'  => 'title_color',
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Text', 'mkdf-core'),
                        'param_name'  => 'text',
                        'admin_label' => true,
                        'description' => ''
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Box Padding', 'mkdf-core'),
                        'param_name'  => 'padding',
                        'admin_label' => true,
                        'group'       => esc_html__('Design Options', 'mkdf-core'),
                        'description' => ''
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => esc_html__('Icon Color', 'mkdf-core'),
                        'param_name'  => 'icon_color',
                        'group'       => esc_html__('Design Options', 'mkdf-core'),
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Icon Size', 'mkdf-core'),
                        'param_name'  => 'icon_size',
                        'admin_label' => true,
                        'group'       => esc_html__('Design Options', 'mkdf-core'),
                        'description' => ''
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => 'Box Color',
                        'param_name'  => 'background_color',
                        'group'       => esc_html__('Design Options', 'mkdf-core'),
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => 'Box Hover Color',
                        'param_name'  => 'background_hover_color',
                        'group'       => esc_html__('Design Options', 'mkdf-core'),
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'checkbox',
                        'heading'     => 'Disable Box Border',
                        'param_name'  => 'disable_border',
                        'value' => array(esc_html__('Disable border?', 'mkdf-core') => 'yes'),
                        'group'       => esc_html__('Design Options', 'mkdf-core'),
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'checkbox',
                        'heading'     => 'Disable Box Rounded Corners',
                        'param_name'  => 'disable_border_radius',
                        'value' => array(esc_html__('Disable Rouded Corners?', 'mkdf-core') => 'yes'),
                        'group'       => esc_html__('Design Options', 'mkdf-core'),
                        'admin_label' => true
                    )
                )
            )
        ));

    }

    /**
     * Renders shortcodes HTML
     *
     * @param $atts array of shortcode params
     * @param $content string shortcode content
     *
     * @return string
     */
    public function render($atts, $content = null) {

        $default_atts = array(
            'title'            => '',
            'title_tag'        => 'h5',
            'text'             => '',
            'padding'             => '',
            'box_type'         => 'inside-icon',
            'icon_color'       => '',
            'title_color'       => '',
            'icon_size'       => '',
            'background_color' => '',
            'background_hover_color' => '',
            'button_position' => 'right',
            'button_link' => '',
            'button_text' => 'button',
            'button_target' => '',
            'custom_icon' => '',
            'button_text_color' => '',
            'button_background_color' => '',
            'button_hover_background_color' => '',
            'button_hover_text_color' => '',
            'button_hover_animation' => '',
            'disable_border' => '',
            'disable_border_radius' => 'no',
            'animate_icon_box' => 'no',
            'animation_type' => 'entire-box'
        );

        $default_atts = array_merge($default_atts, mediclinic_mikado_icon_collections()->getShortcodeParams());
        $params       = shortcode_atts($default_atts, $atts);

        $params['icon_parameters'] = $this->getIconParameters($params);
        $params['holder_classes']  = $this->getHolderClasses($params);
        $params['box_style']       = $this->getBoxStyle($params);
        $params['title_style']       = $this->getTitleStyle($params);
        $params['padding']       = $this->getBoxPadding($params);
        $params['box_hover_style']       = $this->getHoverStyle($params);
        $params['button_parameters'] = $this->getButtonParameters($params);
        $params['custom_icon_styles']     = $this->getCustomIconStyles($params);

        //get correct heading value. If provided heading isn't valid get the default one
        $headings_array      = array('h2', 'h3', 'h4', 'h5', 'h6');
        $params['title_tag'] = (in_array($params['title_tag'], $headings_array)) ? $params['title_tag'] : $default_atts['title_tag'];

        //Get HTML from template
        $html = mkdf_core_get_shortcode_module_template_part('templates/'.$params['box_type'], 'icon-box', '', $params);

        return $html;

    }

    private function getIconParameters($params) {
        $iconPackName = mediclinic_mikado_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);

        $params_array['icon_pack']   = $params['icon_pack'];
        $params_array[$iconPackName] = $params[$iconPackName];

        $params_array['icon_color'] = $params['icon_color'];
        $params_array['custom_size'] = $params['icon_size'];

        return $params_array;
    }

    private function getHolderClasses($params) {
        $classes   = array('mkdf-icon-box-holder');
        $classes[] = $params['box_type'];

        if($params['disable_border'] !== '') {
            $classes[] = 'border-disabled';
        }
        if($params['disable_border_radius'] !== '') {
            $classes[] = 'rounded-corners-disabled';
        }
        if($params['animate_icon_box'] !== '' && $params['animate_icon_box'] == 'yes') {
            $classes[] = 'animate-icon-box';
        }

        if($params['animation_type'] !== '') {
            $classes[] = 'animate-' . $params['animation_type'];
        }

        return $classes;
    }

    private function getBoxStyle($params) {
        $style = '';

        if($params['background_color'] != '') {
            $style = 'background-color:'.$params['background_color'];
        }

        return $style;
    }

    private function getTitleStyle($params) {
        $style = '';

        if($params['title_color'] != '') {
            $style = 'color:'.$params['title_color'];
        }

        return $style;
    }

    private function getBoxPadding($params) {
        $style = '';

        if($params['padding'] != '') {
            $style = 'padding:'.$params['padding'];
        }

        return $style;
    }


    private function getHoverStyle($params) {
        $style = '';

        if($params['background_hover_color'] != '') {
            $style = 'background-color:'.$params['background_hover_color'];
        }

        return $style;
    }


    private function getButtonParameters($params) {
        $button_params_array = array();

        if(!empty($params['button_link'])) {
            $button_params_array['link'] = $params['button_link'];
        }

        if(!empty($params['button_target'])) {
            $button_params_array['target'] = $params['button_target'];
        }

        if(!empty($params['button_text'])) {
            $button_params_array['text'] = $params['button_text'];
        }

        if(!empty($params['button_text_color'])) {
            $button_params_array['color'] = $params['button_text_color'];
        }

        if(!empty($params['button_background_color'])) {
            $button_params_array['background_color'] = $params['button_background_color'];
        }
        if(!empty($params['button_hover_background_color'])) {
            $button_params_array['hover_background_color'] = $params['button_hover_background_color'];
        }
        if(!empty($params['button_hover_text_color'])) {
            $button_params_array['hover_color'] = $params['button_hover_text_color'];
        }

        if(!empty($params['button_hover_background_color'])) {
            $button_params_array['hover_border_color'] = $params['button_hover_background_color'];
        }

        if(!empty($params['button_background_color'])) {
            $button_params_array['border_color'] = $params['button_background_color'];
        }
        if(!empty($params['button_hover_animation'])) {
            $button_params_array['hover_animation'] = $params['button_hover_animation'];
        }

        return $button_params_array;
    }

    private function getCustomIconStyles($params){
        $styles = '';
        if(!empty($params['custom_icon'])) {
            $image_params = mediclinic_mikado_get_uploaded_image_dimensions($params['custom_icon'], true);
            foreach($image_params as $key => $value) {
                $styles .= $key . ':' . $value . ';';
            }
        }

        return $styles;
    }
}