<?php
/**
 * Underline icon box shortcode template
 */
?>
<div <?php mediclinic_mikado_inline_style($box_style); ?> <?php mediclinic_mikado_class_attribute($holder_classes); ?>>
    <?php if($background_hover_color !== '') { ?>
        <div class="mkdf-icon-box-overlay" <?php mediclinic_mikado_inline_style($box_hover_style); ?>></div>
    <?php }?>
    <div class="mkdf-icon-box-holder-inner" <?php mediclinic_mikado_inline_style($padding); ?>>
        <?php if(!empty($custom_icon)) : ?>
            <div class="mkdf-custom-icon-holder">
                <div class="mkdf-custom-icon">
                    <img src="<?php echo wp_get_attachment_image_url($custom_icon, 'full'); ?>" alt="<?php echo esc_attr('Custom icon', 'mkdf-core') ?>" <?php mediclinic_mikado_inline_style($custom_icon_styles); ?>/>
                </div>
            </div>
        <?php else: ?>
            <div class="mkdf-icon-box-icon-holder">
                <?php echo mediclinic_mikado_execute_shortcode('mkdf_icon', $icon_parameters); ?>
            </div>
        <?php endif; ?>
        <div class="mkdf-icon-box-text-holder">
            <?php if(!empty($title)) : ?>
                <div class="mkdf-icon-box-title">
                    <<?php echo esc_attr($title_tag); ?>><?php echo esc_html($title); ?></<?php echo esc_attr($title_tag); ?>>
                </div>
            <?php endif; ?>
            <?php if(!empty($text)) : ?>
                <div class="mkdf-icon-box-text">
                    <?php echo esc_html($text); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>