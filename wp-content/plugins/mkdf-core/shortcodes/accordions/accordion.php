<?php
namespace MikadoCore\CPT\Shortcodes\Accordion;

use MikadoCore\Lib;

class Accordion implements Lib\ShortcodeInterface {
	private $base;

	function __construct() {
		$this->base = 'mkdf_accordion';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return	$this->base;
	}

	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map(
				array(
					'name'                    => esc_html__( 'Mikado Accordion', 'mkdf-core' ),
					'base'                    => $this->base,
					'as_parent'               => array( 'only' => 'mkdf_accordion_tab' ),
					'content_element'         => true,
					'category'                => esc_html__( 'by MIKADO', 'mkdf-core' ),
					'icon'                    => 'icon-wpb-accordion extended-custom-icon',
					'show_settings_on_create' => true,
					'js_view'                 => 'VcColumnView',
					'params'                  => array(
						array(
							'type'       => 'textfield',
							'param_name' => 'el_class',
							'heading'    => esc_html__( 'Custom CSS Class', 'mkdf-core' )
						),
						array(
							'type'       => 'dropdown',
							'param_name' => 'style',
							'heading'    => esc_html__( 'Style', 'mkdf-core' ),
							'value'      => array(
								esc_html__( 'Accordion', 'mkdf-core' ) => 'accordion',
								esc_html__( 'Toggle', 'mkdf-core' )    => 'toggle'
							)
						)
					)
				)
			);
		}
	}
	
	public function render($atts, $content = null) {
		$default_atts = array(
			'el_class'        => '',
			'title'           => '',
			'style'           => 'accordion',
			'layout'          => 'boxed',
			'background_skin' => ''
		);
		
		$params = shortcode_atts($default_atts, $atts);
		extract($params);

		$params['acc_class'] = $this->getAccordionClasses($params);
		$params['content'] = $content;

		$output = '';

		$output .= mkdf_core_get_shortcode_module_template_part('templates/accordion-holder-template','accordions', '', $params);

		return $output;
	}

	/**
	   * Generates accordion classes
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function getAccordionClasses($params){
		$holder_classes = array('mkdf-ac-default');

		switch($params['style']) {
            case 'toggle':
                $holder_classes[] = 'mkdf-toggle';
                break;
            default:
	            $holder_classes[] = 'mkdf-accordion';
                break;
        }

		if (!empty($params['layout'])) {
			$holder_classes[] = 'mkdf-ac-'.esc_attr($params['layout']);
		}

		if (!empty($params['el_class'])) {
			$holder_classes[] = esc_attr($params['el_class']);
		}

        return implode(' ', $holder_classes);
	}
}
