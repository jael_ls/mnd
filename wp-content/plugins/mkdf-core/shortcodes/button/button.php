<?php
namespace MikadoCore\CPT\Shortcodes\Button;

use MikadoCore\Lib;

class Button implements Lib\ShortcodeInterface {
    private $base;
	
    public function __construct() {
        $this->base = 'mkdf_button';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base attribute
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer
     */
    public function vcMap() {
	    if(function_exists('vc_map')) {
		    vc_map(
		    	array(
				    'name'                      => esc_html__( 'Mikado Button', 'mkdf-core' ),
				    'base'                      => $this->base,
				    'category'                  => esc_html__( 'by MIKADO', 'mkdf-core' ),
				    'icon'                      => 'icon-wpb-button extended-custom-icon',
				    'allowed_container_element' => 'vc_row',
				    'params'                    => array_merge(
					    array(
						    array(
							    'type'       => 'textfield',
							    'param_name' => 'custom_class',
							    'heading'    => esc_html__( 'Custom CSS Class', 'mkdf-core' )
						    ),
						    array(
							    'type'        => 'dropdown',
							    'param_name'  => 'type',
							    'heading'     => esc_html__( 'Type', 'mkdf-core' ),
							    'value'       => array(
								    esc_html__( 'Solid', 'mkdf-core' )   => 'solid',
								    esc_html__( 'Outline', 'mkdf-core' ) => 'outline',
								    esc_html__( 'Simple', 'mkdf-core' )  => 'simple'
							    ),
							    'admin_label' => true
						    ),
                            array(
                                'type'        => 'dropdown',
                                'param_name'  => 'solid_skin',
                                'heading'     => esc_html__( 'Type', 'mkdf-core' ),
                                'value'       => array(
                                    esc_html__( 'Dark', 'mkdf-core' )   => 'dark',
                                    esc_html__( 'Light', 'mkdf-core' ) => 'light'
                                ),
                                'admin_label' => true,
                                'dependency' => array( 'element' => 'type', 'value' => array( 'solid' ) )
                            ),
						    array(
							    'type'       => 'dropdown',
							    'param_name' => 'size',
							    'heading'    => esc_html__( 'Size', 'mkdf-core' ),
							    'value'      => array(
								    esc_html__( 'Default', 'mkdf-core' ) => '',
								    esc_html__( 'Small', 'mkdf-core' )   => 'small',
								    esc_html__( 'Medium', 'mkdf-core' )  => 'medium',
								    esc_html__( 'Large', 'mkdf-core' )   => 'large',
								    esc_html__( 'Huge', 'mkdf-core' )    => 'huge'
							    ),
							    'dependency' => array( 'element' => 'type', 'value' => array( 'solid', 'outline' ) )
						    ),
						    array(
							    'type'        => 'textfield',
							    'param_name'  => 'text',
							    'heading'     => esc_html__( 'Text', 'mkdf-core' ),
							    'value'       => esc_html__( 'Button Text', 'mkdf-core' ),
							    'save_always' => true,
							    'admin_label' => true
						    ),
						    array(
							    'type'       => 'textfield',
							    'param_name' => 'link',
							    'heading'    => esc_html__( 'Link', 'mkdf-core' )
						    ),
						    array(
							    'type'        => 'dropdown',
							    'param_name'  => 'target',
							    'heading'     => esc_html__( 'Link Target', 'mkdf-core' ),
							    'value'       => array_flip( mediclinic_mikado_get_link_target_array() ),
							    'save_always' => true
						    )
					    ),
					    mediclinic_mikado_icon_collections()->getVCParamsArray( array(), '', true ),
					    array(
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'color',
							    'heading'    => esc_html__( 'Color', 'mkdf-core' ),
							    'group'      => esc_html__( 'Design Options', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'hover_color',
							    'heading'    => esc_html__( 'Hover Color', 'mkdf-core' ),
							    'group'      => esc_html__( 'Design Options', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'background_color',
							    'heading'    => esc_html__( 'Background Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'type', 'value' => array( 'solid' ) ),
							    'group'      => esc_html__( 'Design Options', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'hover_background_color',
							    'heading'    => esc_html__( 'Hover Background Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'type', 'value' => array( 'solid', 'outline' ) ),
							    'group'      => esc_html__( 'Design Options', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'border_color',
							    'heading'    => esc_html__( 'Border Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'type', 'value' => array( 'outline' ) ),
							    'group'      => esc_html__( 'Design Options', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'hover_border_color',
							    'heading'    => esc_html__( 'Hover Border Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'type', 'value' => array( 'outline' ) ),
							    'group'      => esc_html__( 'Design Options', 'mkdf-core' )
						    ),
                            array(
                                'type'       => 'colorpicker',
                                'param_name' => 'icon_background_color',
                                'heading'    => esc_html__( 'Icon Background Color', 'mkdf-core' ),
                                'dependency' => array( 'element' => 'type', 'value' => array( 'solid' ) ),
                                'group'      => esc_html__( 'Design Options', 'mkdf-core' )
                            ),
						    array(
							    'type'       => 'textfield',
							    'param_name' => 'font_size',
							    'heading'    => esc_html__( 'Font Size (px)', 'mkdf-core' ),
							    'group'      => esc_html__( 'Design Options', 'mkdf-core' )
						    ),
						    array(
							    'type'        => 'dropdown',
							    'param_name'  => 'font_weight',
							    'heading'     => esc_html__( 'Font Weight', 'mkdf-core' ),
							    'value'       => array_flip( mediclinic_mikado_get_font_weight_array( true ) ),
							    'save_always' => true,
							    'group'       => esc_html__( 'Design Options', 'mkdf-core' )
						    ),
						    array(
							    'type'        => 'textfield',
							    'param_name'  => 'margin',
							    'heading'     => esc_html__( 'Margin', 'mkdf-core' ),
							    'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'mkdf-core' ),
							    'group'       => esc_html__( 'Design Options', 'mkdf-core' )
						    ),
						    array(
							    'type'        => 'textfield',
							    'param_name'  => 'padding',
							    'heading'     => esc_html__( 'Button Padding', 'mkdf-core' ),
							    'description' => esc_html__( 'Insert padding in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'mkdf-core' ),
							    'dependency'  => array( 'element' => 'type', 'value' => array( 'solid', 'outline' ) ),
							    'group'       => esc_html__( 'Design Options', 'mkdf-core' )
						    )
					    )
				    )
			    )
		    );
	    }
    }

    /**
     * Renders HTML for button shortcode
     *
     * @param array $atts
     * @param null $content
     *
     * @return string
     */
    public function render($atts, $content = null) {
        $default_atts = array(
            'size'                   => '',
            'type'                   => 'solid',
            'solid_skin'             => 'dark',
            'text'                   => '',
            'link'                   => '',
            'target'                 => '_self',
            'color'                  => '',
            'hover_color'            => '',
            'background_color'       => '',
            'hover_background_color' => '',
            'border_color'           => '',
            'hover_border_color'     => '',
            'icon_background_color'  => '',
            'font_size'              => '',
            'font_weight'            => '',
            'margin'                 => '',
            'padding'                => '',
            'custom_class'           => '',
            'html_type'              => 'anchor',
            'input_name'             => '',
            'custom_attrs'           => array()
        );

        $default_atts = array_merge($default_atts, mediclinic_mikado_icon_collections()->getShortcodeParams());
        $params       = shortcode_atts($default_atts, $atts);


            $iconPackName   = mediclinic_mikado_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);
            $params['icon'] = $iconPackName ? $params[$iconPackName] : '';



        $params['size'] = !empty($params['size']) ? $params['size'] : 'medium';
        $params['type'] = !empty($params['type']) ? $params['type'] : 'solid';


        $params['link']   = !empty($params['link']) ? $params['link'] : '#';
        $params['target'] = !empty($params['target']) ? $params['target'] : $default_atts['target'];

        //prepare params for template
        $params['button_classes']      = $this->getButtonClasses($params);
        $params['button_custom_attrs'] = !empty($params['custom_attrs']) ? $params['custom_attrs'] : array();
        $params['button_styles']       = $this->getButtonStyles($params);
        $params['icon_styles']       = $this->getIconStyles($params);
        $params['button_data']         = $this->getButtonDataAttr($params);

        return mkdf_core_get_shortcode_module_template_part('templates/'.$params['html_type'], 'button', '', $params);
    }

    /**
     * Returns array of button styles
     *
     * @param $params
     *
     * @return array
     */
    private function getButtonStyles($params) {
        $styles = array();

        if(!empty($params['color'])) {
            $styles[] = 'color: '.$params['color'];
        }

        if(!empty($params['background_color']) && $params['type'] !== 'outline') {
            $styles[] = 'background-color: '.$params['background_color'];
        }

        if(!empty($params['border_color'])) {
            $styles[] = 'border-color: '.$params['border_color'];
        }

        if(!empty($params['font_size'])) {
            $styles[] = 'font-size: '.mediclinic_mikado_filter_px($params['font_size']).'px';
        }

        if(!empty($params['font_weight']) && $params['font_weight'] !== '') {
            $styles[] = 'font-weight: '.$params['font_weight'];
        }

        if($params['margin'] !== '') {
            $styles[] = 'margin: '.$params['margin'];
        }
	
	    if($params['padding'] !== '') {
		    $styles[] = 'padding: '.$params['padding'];
	    }

        return $styles;
    }

    /**
     * Returns array of icon styles
     *
     * @param $params
     *
     * @return array
     */
    private function getIconStyles($params) {
        $styles = array();


        if(!empty($params['border_color']) && $params['type'] == 'outline') {
            $styles[] = 'border-left-color: '.$params['border_color'];
        }
        if(!empty($params['icon_background_color']) && $params['type'] == 'solid') {
            $styles[] = 'background-color: '.$params['icon_background_color'];
        }

        return $styles;
    }

    /**
     *
     * Returns array of button data attr
     *
     * @param $params
     *
     * @return array
     */
    private function getButtonDataAttr($params) {
        $data = array();

        if(!empty($params['hover_color'])) {
            $data['data-hover-color'] = $params['hover_color'];
        }

        if(!empty($params['hover_background_color'])) {
            $data['data-hover-bg-color'] = $params['hover_background_color'];
        }

        if(!empty($params['hover_border_color'])) {
            $data['data-hover-border-color'] = $params['hover_border_color'];
        }

        return $data;
    }

    /**
     * Returns array of HTML classes for button
     *
     * @param $params
     *
     * @return array
     */
    private function getButtonClasses($params) {
        $buttonClasses = array(
            'mkdf-btn',
            'mkdf-btn-'.$params['size'],
            'mkdf-btn-'.$params['type']
        );

        if($params['type'] == 'solid') {
            $buttonClasses[] = 'mkdf-btn-solid-' . $params['solid_skin'];
        }

        if(!empty($params['hover_background_color'])) {
            $buttonClasses[] = 'mkdf-btn-custom-hover-bg';
        }

        if(!empty($params['hover_border_color'])) {
            $buttonClasses[] = 'mkdf-btn-custom-border-hover';
        }

        if(!empty($params['hover_color'])) {
            $buttonClasses[] = 'mkdf-btn-custom-hover-color';
        }

        if(!empty($params['icon'])) {
            $buttonClasses[] = 'mkdf-btn-icon';
        }

        if(!empty($params['custom_class'])) {
            $buttonClasses[] = esc_attr($params['custom_class']);
        }

        return $buttonClasses;
    }
}