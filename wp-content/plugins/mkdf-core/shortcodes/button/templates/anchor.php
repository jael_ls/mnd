<a itemprop="url" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>" <?php mediclinic_mikado_inline_style($button_styles); ?> <?php mediclinic_mikado_class_attribute($button_classes); ?> <?php echo mediclinic_mikado_get_inline_attrs($button_data); ?> <?php echo mediclinic_mikado_get_inline_attrs($button_custom_attrs); ?>>
    <span class="mkdf-btn-text"><?php echo esc_html($text); ?></span>
    <span class="mkdf-btn-icon-holder">
        <span class="mkdf-btn-icon-normal" <?php mediclinic_mikado_inline_style($icon_styles); ?>><?php echo mediclinic_mikado_icon_collections()->renderIcon($icon, $icon_pack); ?></span>
        <span class="mkdf-btn-icon-flip" <?php mediclinic_mikado_inline_style($icon_styles); ?>><?php echo mediclinic_mikado_icon_collections()->renderIcon($icon, $icon_pack); ?></span>
    </span>
</a>