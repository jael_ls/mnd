<?php
namespace MikadoCore\CPT\Shortcodes\EllipticalSlider;

use MikadoCore\Lib;

class EllipticalSlide implements Lib\ShortcodeInterface {

    private $base;

    function __construct() {
        $this->base = 'mkdf_elliptical_slide';
		add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
                'name' => esc_html__('Mikado Elliptical Slide','mkdf-core'),
                'base' => $this->base,
                'as_child' => array('only' => 'mkdf_elliptical_slider'),
				'as_parent' => array('except' => 'vc_tabs, vc_accordion, cover_boxes, portfolio_list, portfolio_slider, mkd_carousel'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
				'category' => esc_html__('by MIKADO','mkdf-core'),
                'icon' => 'icon-wpb-elliptical-slide extended-custom-icon',
				'js_view' => 'VcColumnView',
				'content_element'	=> true,
                'params' => array(
					array(
						'type'       => 'attach_image',
						'heading'    => esc_html__('Image','mkdf-core'),
						'param_name' => 'image'
					)
				)
        ));
    }

    public function render($atts, $content = null) {

        $args = array(
            'image' => '',
            'elliptical_section_background_color' => ''
        );

        $params = shortcode_atts($args, $atts);

        $params['image'] = $this->getImageSrc($params);
        $params['content'] = $content;
        $params['background_image'] = $this->getBackgroundImage($params);


        $html = mkdf_core_get_shortcode_module_template_part('templates/elliptical-slide-template', 'elliptical-slider', '', $params);

        return $html;
    }



    private function getImageSrc($params) {


        if (is_numeric($params['image'])) {
            $image_src = wp_get_attachment_url($params['image']);
        } else {
            $image_src = $params['image'];
        }

        return $image_src;

    }
	
	private function getBackgroundImage($params) {

		$style = array();

		if (!empty($params['image'])) {
			$style[] = 'background-image:url(' .$this->getImageSrc($params).')';
		}

		return implode(';', $style);

	}
}