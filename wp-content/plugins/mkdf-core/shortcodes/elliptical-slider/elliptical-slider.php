<?php
namespace MikadoCore\CPT\Shortcodes\EllipticalSlider;

use MikadoCore\Lib;

class EllipticalSlider implements Lib\ShortcodeInterface {

    private $base;

    function __construct() {
        $this->base = 'mkdf_elliptical_slider';
		add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
                'name' => esc_html__('Mikado Elliptical Slider','mkdf-core'),
                'base' => $this->base,
                'icon' => 'icon-wpb-elliptical-slide extended-custom-icon',
                'category' => esc_html__('by MIKADO','mkdf-core'),
                'as_parent' => array('only' => 'mkdf_elliptical_slide'),
				'content_element'	=> true,
                'js_view' => 'VcColumnView',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Animation speed','mkdf-core'),
                        'admin_label' => true,
                        'param_name' => 'animation_speed',
                        'value' => '',
                        'description' => esc_html__('Speed of slide animation in miliseconds','mkdf-core')
                    )
                )
            )
        );
    }

    public function render($atts, $content = null) {

        $args = array(
            'animation_speed' => ''
        );

        $params = shortcode_atts($args, $atts);
        extract($params);

        $data_attr = $this->getDataParams($params);

        $html = '';
        $html .= '<div class="mkdf-elliptical-slider">';
        $html .= '<div class="mkdf-elliptical-slider-slides" ' . mediclinic_mikado_get_inline_attrs($data_attr). '>';
        	$html .= do_shortcode($content);
        $html.= '</div>';
        $html.= '</div>';

        return $html;
    }

    private function getDataParams($params){
        $data_attr =  array();
        if(!empty($params['animation_speed'])){
            $data_attr['data-animation-speed'] =  $params['animation_speed'];
        }

        return $data_attr;
    }
}