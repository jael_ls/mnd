<?php
/**
 * Eliptic Slider shortcode template
 */
?>

<div class="mkdf-elliptical-slide" <?php echo mediclinic_mikado_inline_style($background_image)?>>
    <div class="mkdf-elliptical-slide-image-holder-wrapper">
        <span class="mkdf-elliptical-slide-image-holder">
            <img src="<?php echo esc_url($image); ?>" alt="<?php esc_attr_e( 'mkdf-eliptic-slider', 'mediclinic' ); ?>"/>
        </span>
    </div>
    <div class="mkdf-elliptical-slide-content-holder" >
        <div class="mkdf-elliptical-slide-content-holder-inner grid_section">
            <div class="mkdf-elliptical-slide-content-wrapper mkdf-container-inner">
                <div class="mkdf-elliptical-slide-wrapper-inner">
					<div class="mkdf-elliptical-slide-svg-holder">
						<svg version="1.1" class="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  width="120" height="660" viewBox="0 0 97 604" enable-background="new 0 0 97 604"  xml:space="preserve">
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#43D5CB" d="M0,0c36,93.5,56,195.4,56,302c0,106.6-20,208.5-56,302 c22.8,0,97,0,97,0V0C97,0,32.5,0,0,0z"/>
						</svg>
					</div>
                    <div class="mkdf-elliptical-slide-elements-holder">

						<?php echo do_shortcode($content); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>