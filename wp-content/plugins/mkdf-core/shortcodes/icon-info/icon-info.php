<?php
namespace MikadoCore\CPT\Shortcodes\IconInfo;

use MikadoCore\Lib;

class IconInfo implements Lib\ShortcodeInterface {
    private $base;
	
    public function __construct() {
        $this->base = 'mkdf_icon_info';

        add_action('vc_before_init', array($this, 'vcMap'));
    }
	
    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
	    if(function_exists('vc_map')) {
		    vc_map(
		    	array(
				    'name'                      => esc_html__( 'Mikado Icon Info', 'mkdf-core' ),
				    'base'                      => $this->base,
				    'icon'                      => 'icon-wpb-icon-info extended-custom-icon',
				    'category'                  => esc_html__( 'by MIKADO', 'mkdf-core' ),
				    'allowed_container_element' => 'vc_row',
				    'params'                    => array_merge(
					    mediclinic_mikado_icon_collections()->getVCParamsArray(),
					    array(
						    array(
							    'type'       => 'attach_image',
							    'param_name' => 'custom_icon',
							    'heading'    => esc_html__( 'Custom Icon', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'dropdown',
							    'param_name' => 'icon_type',
							    'heading'    => esc_html__( 'Icon Type', 'mkdf-core' ),
							    'value'      => array(
								    esc_html__( 'Normal', 'mkdf-core' ) => 'mkdf-normal',
								    esc_html__( 'Circle', 'mkdf-core' ) => 'mkdf-circle',
								    esc_html__( 'Square', 'mkdf-core' ) => 'mkdf-square'
							    ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'dropdown',
							    'param_name' => 'icon_size',
							    'heading'    => esc_html__( 'Icon Size', 'mkdf-core' ),
							    'value'      => array(
								    esc_html__( 'Medium', 'mkdf-core' )     => 'mkdf-icon-medium',
								    esc_html__( 'Tiny', 'mkdf-core' )       => 'mkdf-icon-tiny',
								    esc_html__( 'Small', 'mkdf-core' )      => 'mkdf-icon-small',
								    esc_html__( 'Large', 'mkdf-core' )      => 'mkdf-icon-large',
								    esc_html__( 'Very Large', 'mkdf-core' ) => 'mkdf-icon-huge'
							    ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'textfield',
							    'param_name' => 'custom_icon_size',
							    'heading'    => esc_html__( 'Custom Icon Size (px)', 'mkdf-core' ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'textfield',
							    'param_name' => 'shape_size',
							    'heading'    => esc_html__( 'Shape Size (px)', 'mkdf-core' ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'icon_color',
							    'heading'    => esc_html__( 'Icon Color', 'mkdf-core' ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'icon_hover_color',
							    'heading'    => esc_html__( 'Icon Hover Color', 'mkdf-core' ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'icon_background_color',
							    'heading'    => esc_html__( 'Icon Background Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'icon_type', 'value'   => array( 'mkdf-square', 'mkdf-circle' ) ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'icon_hover_background_color',
							    'heading'    => esc_html__( 'Icon Hover Background Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'icon_type', 'value'   => array( 'mkdf-square', 'mkdf-circle' ) ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'icon_border_color',
							    'heading'    => esc_html__( 'Icon Border Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'icon_type', 'value'   => array( 'mkdf-square', 'mkdf-circle' ) ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'icon_border_hover_color',
							    'heading'    => esc_html__( 'Icon Border Hover Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'icon_type', 'value'   => array( 'mkdf-square', 'mkdf-circle' ) ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'textfield',
							    'param_name' => 'icon_border_width',
							    'heading'    => esc_html__( 'Border Width (px)', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'icon_type', 'value'   => array( 'mkdf-square', 'mkdf-circle' ) ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'dropdown',
							    'param_name' => 'icon_animation',
							    'heading'    => esc_html__( 'Icon Animation', 'mkdf-core' ),
							    'value'      => array_flip( mediclinic_mikado_get_yes_no_select_array( false ) ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'textfield',
							    'param_name' => 'icon_animation_delay',
							    'heading'    => esc_html__( 'Icon Animation Delay (ms)', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'icon_animation', 'value' => array( 'yes' ) ),
							    'group'      => esc_html__( 'Icon Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'textfield',
							    'param_name' => 'title',
							    'heading'    => esc_html__( 'Title', 'mkdf-core' )
						    ),
						    array(
							    'type'        => 'dropdown',
							    'param_name'  => 'title_tag',
							    'heading'     => esc_html__( 'Title Tag', 'mkdf-core' ),
							    'value'       => array_flip( mediclinic_mikado_get_title_tag( true ) ),
							    'save_always' => true,
							    'dependency'  => array( 'element' => 'title', 'not_empty' => true ),
							    'group'       => esc_html__( 'Text Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'title_color',
							    'heading'    => esc_html__( 'Title Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'title', 'not_empty' => true ),
							    'group'      => esc_html__( 'Text Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'textarea',
							    'param_name' => 'subtitle',
							    'heading'    => esc_html__( 'Subtitle', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'colorpicker',
							    'param_name' => 'subtitle_color',
							    'heading'    => esc_html__( 'Subtitle Color', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'text', 'not_empty' => true ),
							    'group'      => esc_html__( 'Text Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'textfield',
							    'param_name' => 'subtitle_top_margin',
							    'heading'    => esc_html__( 'Subtitle Top Margin (px)', 'mkdf-core' ),
							    'dependency' => array( 'element' => 'text', 'not_empty' => true ),
							    'group'      => esc_html__( 'Text Settings', 'mkdf-core' )
						    ),
						    array(
							    'type'        => 'textfield',
							    'param_name'  => 'link',
							    'heading'     => esc_html__( 'Link', 'mkdf-core' ),
							    'description' => esc_html__( 'Set link around icon and title', 'mkdf-core' )
						    ),
						    array(
							    'type'       => 'dropdown',
							    'param_name' => 'target',
							    'heading'    => esc_html__( 'Target', 'mkdf-core' ),
							    'value'      => array_flip( mediclinic_mikado_get_link_target_array() ),
							    'dependency' => array( 'element' => 'link', 'not_empty' => true ),
						    )
					    )
				    )
			    )
		    );
	    }
    }

    /**
     * @param array $atts
     * @param null $content
     *
     * @return string
     */
    public function render($atts, $content = null) {
        $default_atts = array(
            'custom_icon'                 => '',
            'icon_type'                   => 'mkdf-normal',
            'icon_size'                   => 'mkdf-icon-medium',
            'custom_icon_size'            => '',
            'shape_size'                  => '',
            'icon_color'                  => '',
            'icon_hover_color'            => '',
            'icon_background_color'       => '',
            'icon_hover_background_color' => '',
            'icon_border_color'           => '',
            'icon_border_hover_color'     => '',
            'icon_border_width'           => '',
            'icon_animation'              => '',
            'icon_animation_delay'        => '',
            'title'                       => '',
            'title_tag'                   => 'h5',
            'title_color'                 => '',
            'title_font_size'             => '',
            'title_font_weight'           => '',
            'subtitle'                    => '',
            'subtitle_color'              => '',
	        'subtitle_top_margin'         => '',
            'link'                        => '',
            'target'                      => '_self',
        );

        $default_atts = array_merge($default_atts, mediclinic_mikado_icon_collections()->getShortcodeParams());
        $params       = shortcode_atts($default_atts, $atts);
	
//	    $params['type'] = !empty($params['type']) ? $params['type'] : $default_atts['type'];

        $params['icon_parameters'] = $this->getIconParameters($params);
        $params['holder_classes']  = $this->getHolderClasses($params);
	    $params['title_styles']    = $this->getTitleStyles($params);
	    $params['subtitle_styles']    = $this->getSubtitleStyles($params);
	    $params['title_tag']       = !empty($params['title_tag']) ? $params['title_tag'] : $default_atts['title_tag'];
        $params['text_styles']     = $this->getTextStyles($params);
	    $params['target']          = !empty($params['target']) ? $params['target'] : $default_atts['target'];
	    
		return mkdf_core_get_shortcode_module_template_part('templates/icon-info', 'icon-info', '', $params);
    }

    /**
     * Returns parameters for icon shortcode as a string
     *
     * @param $params
     *
     * @return array
     */
    private function getIconParameters($params) {
        $params_array = array();

        if(empty($params['custom_icon'])) {
            $iconPackName = mediclinic_mikado_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);

            $params_array['icon_pack']   = $params['icon_pack'];
            $params_array[$iconPackName] = $params[$iconPackName];

            if(!empty($params['icon_size'])) {
                $params_array['size'] = $params['icon_size'];
            }

            if(!empty($params['custom_icon_size'])) {
                $params_array['custom_size'] = mediclinic_mikado_filter_px($params['custom_icon_size']).'px';
            }

            if(!empty($params['icon_type'])) {
                $params_array['type'] = $params['icon_type'];
            }
	
	        if(!empty($params['shape_size'])) {
		        $params_array['shape_size'] = mediclinic_mikado_filter_px($params['shape_size']).'px';
	        }

            if(!empty($params['icon_border_color'])) {
                $params_array['border_color'] = $params['icon_border_color'];
            }

            if(!empty($params['icon_border_hover_color'])) {
                $params_array['hover_border_color'] = $params['icon_border_hover_color'];
            }

            if($params['icon_border_width'] !== '') {
                $params_array['border_width'] = mediclinic_mikado_filter_px($params['icon_border_width']).'px';
            }

            if(!empty($params['icon_background_color'])) {
                $params_array['background_color'] = $params['icon_background_color'];
            }

            if(!empty($params['icon_hover_background_color'])) {
                $params_array['hover_background_color'] = $params['icon_hover_background_color'];
            }

            $params_array['icon_color'] = $params['icon_color'];

            if(!empty($params['icon_hover_color'])) {
                $params_array['hover_icon_color'] = $params['icon_hover_color'];
            }

            $params_array['icon_animation']       = $params['icon_animation'];
            $params_array['icon_animation_delay'] = $params['icon_animation_delay'];
        }

        return $params_array;
    }

    /**
     * Returns array of holder classes
     *
     * @param $params
     *
     * @return array
     */
    private function getHolderClasses($params) {
        $classes = array('mkdf-info-icon', 'clearfix');


        if(!empty($params['icon_size'])) {
            $classes[] = 'mkdf-icon-info-'.str_replace('mkdf-', '', $params['icon_size']);
        }

        return $classes;
    }

	/**
     * Returns inline title styles
     *
     * @param $params
     *
     * @return string
     */
    private function getTitleStyles($params) {
        $styles = array();

        if(!empty($params['title_color'])) {
			$styles[] = 'color: '.$params['title_color'];
		}

		if(!empty($params['title_font_size'])) {
			$styles[] = 'font-size: '.mediclinic_mikado_filter_px($params['title_font_size']).'px';
		}

		if(!empty($params['title_font_weight'])) {
			$styles[] = 'font-weight: '.$params['title_font_weight'];
		}

        return implode(';', $styles);
    }

	/**
	 * Returns inline subtitle styles
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getSubtitleStyles($params) {
		$styles = array();

		if(!empty($params['subtitle_color'])) {
			$styles[] = 'color: '.$params['subtitle_color'];
		}


		return implode(';', $styles);
	}

	/**
     * Returns inline text styles
     *
     * @param $params
     *
     * @return string
     */
    private function getTextStyles($params) {
        $styles = array();

        if(!empty($params['text_color'])) {
            $styles[] = 'color: '.$params['text_color'];
        }
	    
	    if(!empty($params['text_top_margin'])) {
		    $styles[] = 'margin-top: '.mediclinic_mikado_filter_px($params['text_top_margin']).'px';
	    }

        return implode(';', $styles);
    }
}