<div <?php mediclinic_mikado_class_attribute($holder_classes); ?>>
	<div class="mkdf-icon-info-icon">
		<?php if(!empty($link)) : ?>
			<a itemprop="url" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
		<?php endif; ?>
			<?php if(!empty($custom_icon)) : ?>
				<?php echo wp_get_attachment_image($custom_icon, 'full'); ?>
			<?php else: ?>
				<?php echo mkdf_core_get_shortcode_module_template_part('templates/icon', 'icon-info', '', array('icon_parameters' => $icon_parameters)); ?>
			<?php endif; ?>
		<?php if(!empty($link)) : ?>
			</a>
		<?php endif; ?>
	</div>
	<div class="mkdf-info-icon-content">
		<?php if(!empty($title)) { ?>
				<?php if(!empty($link)) : ?>
					<a itemprop="url" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
				<?php endif; ?>
				<span class="mkdf-info-icon-title-text" <?php mediclinic_mikado_inline_style($title_styles); ?>><?php echo esc_html($title); ?></span>
				<?php if(!empty($link)) : ?>
					</a>
				<?php endif; ?>
			<?php if($subtitle !== ''){ ?>
				<span class="mkdf-info-icon-subtitle-text" <?php mediclinic_mikado_inline_style($subtitle_styles); ?>><?php echo esc_html($subtitle); ?></span>
		<?php }} ?>
	</div>
</div>