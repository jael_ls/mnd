<div class="mkdf-counter-holder">
	<div class="mkdf-counter-inner">
		<?php if(!empty($custom_icon) && $enable_icon == 'custom_icon' ) { ?>
				<span class="mkdf-counter-icon"> <?php echo wp_get_attachment_image($custom_icon, 'full'); ?> </span>
		<?php } ?>
		<?php if ( $enable_icon == 'icon_pack'  ) {?>
			<span class="mkdf-counter-icon"> <?php echo mediclinic_mikado_execute_shortcode('mkdf_icon', $icon_attrs); ?></span>
		<?php } ?>
		<?php if(!empty($digit)) { ?>
			<span class="mkdf-counter <?php echo esc_attr($type) ?>" <?php echo mediclinic_mikado_get_inline_style($counter_styles); ?>><?php echo esc_html($digit); ?></span>
		<?php } ?>
		<?php if(!empty($title)) { ?>
			<<?php echo esc_attr($title_tag); ?> class="mkdf-counter-title" <?php echo mediclinic_mikado_get_inline_style($counter_title_styles); ?>>
				<?php echo esc_html($title); ?>
			</<?php echo esc_attr($title_tag); ?>>
		<?php } ?>
		<?php if(!empty($text)) { ?>
			<p class="mkdf-counter-text" <?php echo mediclinic_mikado_get_inline_style($counter_text_styles); ?>><?php echo esc_html($text); ?></p>
		<?php } ?>
	</div>
</div>