<?php
$tab_data_str = '';
$icon_html = '';
$tab_data_str .= 'data-icon-pack="'.$icon_pack.'" ';
$icon_html .= (empty($custom_icon))  ? mediclinic_mikado_execute_shortcode('mkdf_icon', $icon_parameters) : $custom_icon_html;
$tab_data_str .= 'data-icon-html="'. esc_attr($icon_html) .'"';
?>
<div class="mkdf-icon-tab-container" id="tab-<?php echo sanitize_title($tab_title); ?>" <?php print $tab_data_str?>><?php echo do_shortcode($content); ?></div>