<?php
namespace MikadoCore\CPT\Shortcodes\IconTabs;

use MikadoCore\Lib;

class IconTabs implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_icon_tabs';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map(
				array(
					'name'            => esc_html__( 'Mikado Icon Tabs', 'mkdf-core' ),
					'base'            => $this->getBase(),
					'as_parent'       => array( 'only' => 'mkdf_icon_tabs_item' ),
					'content_element' => true,
					'category'        => esc_html__( 'by MIKADO', 'mkdf-core' ),
					'icon'            => 'icon-wpb-icon-tabs extended-custom-icon',
					'js_view'         => 'VcColumnView',
					'params'          => array(
						array(
							'type'        => 'dropdown',
							'param_name'  => 'type',
							'heading'     => esc_html__( 'Number of tabs', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'One', 'mkdf-core' ) => 'one',
								esc_html__( 'Two', 'mkdf-core' ) => 'two',
								esc_html__( 'Three', 'mkdf-core' ) => 'three',
								esc_html__( 'Four', 'mkdf-core' ) => 'four',
								esc_html__( 'Five', 'mkdf-core' ) => 'five',
								esc_html__( 'Six', 'mkdf-core' ) => 'six',
							),
							'save_always' => true
						)
					)
				)
			);
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'type' => 'three'
		);

        $params  = shortcode_atts($args, $atts);
		extract($params);
		
		// Extract tab titles
		preg_match_all('/tab_title="([^\"]+)"/i', $content, $matches, PREG_OFFSET_CAPTURE);
		$tab_titles = array();

		/**
		 * get tab titles array
		 */
		if (isset($matches[0])) {
			$tab_titles = $matches[0];
		}

		$tab_title_array = array();

		foreach($tab_titles as $tab) {
			preg_match('/tab_title="([^\"]+)"/i', $tab[0], $tab_matches, PREG_OFFSET_CAPTURE);
			$tab_title_array[] = $tab_matches[1][0];
		}
		
		$params['holder_classes'] = $this->getHolderClasses($params);
		$params['tabs_titles']    = $tab_title_array;
		$params['content']        = $content;
		
		$output = '';
		
		$output .= mkdf_core_get_shortcode_module_template_part('templates/icon-tab-template','icon-tabs', '', $params);
		
		return $output;
	}
	
	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses($params){
		$holder_classes = array();
		
		$holder_classes[] = !empty($params['type']) ? 'mkdf-icon-tabs-'.esc_attr($params['type']) : 'mkdf-icon-tabs-three';
		
		return implode(' ', $holder_classes);
	}
}