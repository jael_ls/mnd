<?php
namespace MikadoCore\CPT\Shortcodes\Boxes;

use MikadoCore\Lib;

class Boxes implements Lib\ShortcodeInterface {
    private $base;

    function __construct()
    {
        $this->base = 'mkdf_boxes';
        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase()
    {
        return $this->base;
    }

    public function vcMap() {
        if(function_exists('vc_map')) {
            vc_map(
                array(
                    'name'      => esc_html__( 'Mikado Boxes', 'mkdf-core' ),
                    'base'      => $this->base,
                    'icon'      => 'icon-wpb-boxes extended-custom-icon',
                    'category'  => esc_html__( 'by MIKADO', 'mkdf-core' ),
                    'as_parent' => array( 'only' => 'mkdf_boxes_item' ),
                    'js_view'   => 'VcColumnView',
                    'params'    => array(
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'number_of_columns',
                            'heading'     => esc_html__( 'Columns', 'mkdf-core' ),
                            'value'       => array(
                                esc_html__( '1 Column', 'mkdf-core' )  => 'one-column',
                                esc_html__( '2 Columns', 'mkdf-core' ) => 'two-columns',
                                esc_html__( '3 Columns', 'mkdf-core' ) => 'three-columns',
                                esc_html__( '4 Columns', 'mkdf-core' ) => 'four-columns'
                            ),
                            'save_always' => true
                        )
                    )
                )
            );
        }
    }

    public function render($atts, $content = null) {
        $args = array(
            'number_of_columns' 	=> ''
        );

        $params = shortcode_atts($args, $atts);
        extract($params);

        $html = '';

        $boxes_classes = array();
        $boxes_classes[] = 'mkdf-boxes';
        $boxes_style = '';

        if($number_of_columns != ''){
            $boxes_classes[] .= 'mkdf-'.$number_of_columns ;
        }

        $boxes_class = implode(' ', $boxes_classes);

        $html .= '<div ' . mediclinic_mikado_get_class_attribute($boxes_class) . ' ' . mediclinic_mikado_get_inline_attr($boxes_style, 'style'). '>';
        $html .= do_shortcode($content);
        $html .= '</div>';

        return $html;
    }

}