<<?php echo esc_attr($title_tag); ?> class="mkdf-custom-font-holder" <?php mediclinic_mikado_inline_style($holder_styles); ?> <?php echo esc_attr($holder_data); ?>>
	<?php echo esc_html($title); ?>
</<?php echo esc_attr($title_tag); ?>>