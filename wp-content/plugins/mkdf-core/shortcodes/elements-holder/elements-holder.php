<?php
namespace MikadoCore\CPT\Shortcodes\ElementsHolder;

use MikadoCore\Lib;

class ElementsHolder implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_elements_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map(
				array(
					'name'      => esc_html__( 'Mikado Elements Holder', 'mkdf-core' ),
					'base'      => $this->base,
					'icon'      => 'icon-wpb-elements-holder extended-custom-icon',
					'category'  => esc_html__( 'by MIKADO', 'mkdf-core' ),
					'as_parent' => array( 'only' => 'mkdf_elements_holder_item' ),
					'js_view'   => 'VcColumnView',
					'params'    => array(
						array(
							'type'       => 'colorpicker',
							'param_name' => 'background_color',
							'heading'    => esc_html__( 'Background Color', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'number_of_columns',
							'heading'     => esc_html__( 'Columns', 'mkdf-core' ),
							'value'       => array(
								esc_html__( '1 Column', 'mkdf-core' )  => 'one-column',
								esc_html__( '2 Columns', 'mkdf-core' ) => 'two-columns',
								esc_html__( '3 Columns', 'mkdf-core' ) => 'three-columns',
								esc_html__( '4 Columns', 'mkdf-core' ) => 'four-columns',
								esc_html__( '5 Columns', 'mkdf-core' ) => 'five-columns',
								esc_html__( '6 Columns', 'mkdf-core' ) => 'six-columns'
							),
							'save_always' => true
						),
						array(
							'type'       => 'checkbox',
							'param_name' => 'items_float_left',
							'heading'    => esc_html__( 'Items Float Left', 'mkdf-core' ),
							'value'      => array( 'Make Items Float Left?' => 'yes' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'switch_to_one_column',
							'heading'     => esc_html__( 'Switch to One Column', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Default', 'mkdf-core' )      => '',
								esc_html__( 'Below 1280px', 'mkdf-core' ) => '1280',
								esc_html__( 'Below 1024px', 'mkdf-core' ) => '1024',
								esc_html__( 'Below 768px', 'mkdf-core' )  => '768',
								esc_html__( 'Below 600px', 'mkdf-core' )  => '600',
								esc_html__( 'Below 480px', 'mkdf-core' )  => '480',
								esc_html__( 'Never', 'mkdf-core' )        => 'never'
							),
							'description' => esc_html__( 'Choose on which stage item will be in one column', 'mkdf-core' ),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'alignment_one_column',
							'heading'     => esc_html__( 'Choose Alignment In Responsive Mode', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Default', 'mkdf-core' ) => '',
								esc_html__( 'Left', 'mkdf-core' )    => 'left',
								esc_html__( 'Center', 'mkdf-core' )  => 'center',
								esc_html__( 'Right', 'mkdf-core' )   => 'right'
							),
							'description' => esc_html__( 'Alignment When Items are in One Column', 'mkdf-core' ),
							'save_always' => true
						)
					)
				)
			);
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'number_of_columns' 	=> '',
			'switch_to_one_column'	=> '',
			'alignment_one_column' 	=> '',
			'items_float_left' 		=> '',
			'background_color' 		=> ''
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html = '';

		$elements_holder_classes = array();
		$elements_holder_classes[] = 'mkdf-elements-holder';
		$elements_holder_style = '';

		if($number_of_columns != ''){
			$elements_holder_classes[] .= 'mkdf-'.$number_of_columns ;
		}

		if($switch_to_one_column != ''){
			$elements_holder_classes[] = 'mkdf-responsive-mode-' . $switch_to_one_column ;
		} else {
			$elements_holder_classes[] = 'mkdf-responsive-mode-768' ;
		}

		if($alignment_one_column != ''){
			$elements_holder_classes[] = 'mkdf-one-column-alignment-' . $alignment_one_column ;
		}

		if($items_float_left !== ''){
			$elements_holder_classes[] = 'mkdf-ehi-float';
		}

		if($background_color != ''){
			$elements_holder_style .= 'background-color:'. $background_color . ';';
		}

		$elements_holder_class = implode(' ', $elements_holder_classes);

		$html .= '<div ' . mediclinic_mikado_get_class_attribute($elements_holder_class) . ' ' . mediclinic_mikado_get_inline_attr($elements_holder_style, 'style'). '>';
			$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;
	}
}
