<div class="mkdf-image-with-text-holder <?php echo esc_attr($holder_classes); ?>">

        <?php if ($image_behavior === 'lightbox') { ?>
            <a itemprop="image" href="<?php echo esc_url($image['url']); ?>" data-rel="prettyPhoto[iwt_pretty_photo]" title="<?php echo esc_attr($image['alt']); ?>">
        <?php } else if ($image_behavior === 'custom-link' && !empty($custom_link)) { ?>
	            <a itemprop="url" href="<?php echo esc_url($custom_link); ?>" target="<?php echo esc_attr($custom_link_target); ?>">
        <?php } ?>
            <div class="mkdf-iwt-image">
                <?php echo wp_get_attachment_image($image['image_id'], 'full'); ?>
            </div>
            <div class="mkdf-iwt-text-holder">
                <?php if(!empty($title)) { ?>
                    <<?php echo esc_attr($title_tag); ?> class="mkdf-iwt-title" <?php echo mediclinic_mikado_get_inline_style($title_styles); ?>><?php echo esc_html($title); ?></<?php echo esc_attr($title_tag); ?>>
                <?php } ?>
                <?php if(!empty($text)) { ?>
                    <p class="mkdf-iwt-text" <?php echo mediclinic_mikado_get_inline_style($text_styles); ?>><?php echo esc_html($text); ?></p>
                <?php } ?>
            </div>
        <?php if ($image_behavior === 'lightbox' || $image_behavior === 'custom-link') { ?>
            </a>
        <?php } ?>

</div>