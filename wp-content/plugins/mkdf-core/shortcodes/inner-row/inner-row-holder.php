<?php
namespace MikadoCore\CPT\Shortcodes\InnerRowHolder;

use MikadoCore\Lib;

class InnerRowHolder implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_inner_row_holder';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'      => esc_html__( 'Mikado Inner Row Holder', 'mkdf-core' ),
					'base'      => $this->base,
					'icon'      => 'icon-wpb-inner-row-holder extended-custom-icon',
					'category'  => esc_html__( 'by MIKADO', 'mkdf-core' ),
					'as_parent' => array( 'only' => 'mkdf_inner_row_column' ),
					'js_view'   => 'VcColumnView',
					'params'    => array(
						array(
							'type'        => 'dropdown',
							'param_name'  => 'space_between_columns',
							'heading'     => esc_html__( 'Space Between Columns', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Large', 'mkdf-core' )    => 'large',
								esc_html__( 'Medium', 'mkdf-core' )   => 'medium',
								esc_html__( 'Normal', 'mkdf-core' )   => 'normal',
								esc_html__( 'Small', 'mkdf-core' )    => 'small',
								esc_html__( 'Tiny', 'mkdf-core' )     => 'tiny',
								esc_html__( 'No Space', 'mkdf-core' ) => 'no'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'text_alignment',
							'heading'     => esc_html__( 'Horizontal Alignment', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Default', 'mkdf-core' ) => '',
								esc_html__( 'Left', 'mkdf-core' )    => 'left',
								esc_html__( 'Center', 'mkdf-core' )  => 'center',
								esc_html__( 'Right', 'mkdf-core' )   => 'right'
							),
							'save_always' => true
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'space_between_columns' => 'normal',
			'text_alignment'        => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['holder_classes'] = $this->getHolderClasses( $params, $args );
		
		$html = '';
		
		$html .= '<div class="mkdf-inner-row-holder mkdf-grid-row ' . esc_attr( $params['holder_classes'] ) . '">';
		$html .= do_shortcode( $content );
		$html .= '</div>';
		
		return $html;
	}
	
	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses( $params, $args ) {
		$holderClasses = '';
		
		$holderClasses .= ! empty( $params['space_between_columns'] ) ? ' mkdf-grid-' . $params['space_between_columns'] . '-gutter' : ' mkdf-grid-' . $args['space_between_items'] . '-gutter';
		$holderClasses .= ! empty( $params['text_alignment'] ) ? ' mkdf-ir-alignment-' . $params['text_alignment'] : '';
		
		return $holderClasses;
	}
}
