<div class="mkdf-events-content">
    <ol>
        <?php echo do_shortcode($content); ?>
    </ol>
</div> <!-- .events-content -->
<div class="mkdf-timeline">
    <div class="mkdf-events-wrapper">
        <div class="mkdf-events">
            <ol>
                <?php foreach($timeline_params as $key=>$value) { ?>
                    <li><a href="javascript:void(0)" data-date="<?php echo esc_attr($key) ?>"><span class="mkdf-event-text"><?php echo esc_html($value); ?></span><span class="circle-outer"><span class="circle-inner"></span></span></a></li>
                <?php } ?>
            </ol>
            <span class="mkdf-filling-line" aria-hidden="true"></span>
        </div> <!-- .events -->
    </div> <!-- .events-wrapper -->

    <ul class="mkdf-timeline-navigation">
        <li><a href="#0" class="mkdf-prev inactive"></a></li>
        <li><a href="#0" class="mkdf-next"></a></li>
    </ul> <!-- .cd-timeline-navigation -->
</div> <!-- .timeline -->