<?php
namespace MikadoCore\CPT\Shortcodes\HorizontalTimelineItem;

use MikadoCore\Lib;

class HorizontalTimelineItem implements Lib\ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    /**
     * Sets base attribute and registers shortcode with Visual Composer
     */
    public function __construct() {
        $this->base = 'mkdf_horizontal_timeline_item';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base attribute
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer
     */
    public function vcMap() {
        vc_map(array(
            'name'                      => esc_html__('Mikado Horizontal Timeline Item', 'mkdf-core'),
            'base'                      => $this->base,
            'category'                  => esc_html__( 'by MIKADO', 'mkdf-core' ),
            'icon'                      => 'icon-wpb-horizontal-timeline-item extended-custom-icon',
            'js_view'                   => 'VcColumnView',
            'as_child'                  => array('only' => 'mkdf_horizontal_timeline'),
            'as_parent' => array('only' => 'vc_single_image, mkdf_button, mkdf_icon, mkdf_icon_list_item, mkdf_icon_with_text, mkdf_list_ordered, mkdf_progress_bar, mkdf_separator, mkdf_unordered_list, mkdf_video_button, vc_empty_space, vc_column_text, mkdf_custom_font'),
            'params'                    => array(
                array(
                    'type' => 'textfield',
                    'class' => '',
                    'heading' => esc_html__('Timeline Label', 'mkdf-core'),
                    'param_name' => 'timeline_label'
                ),
                array(
                    'type' => 'textfield',
                    'class' => '',
                    'heading' => esc_html__('Timeline Date', 'mkdf-core'),
                    'param_name' => 'timeline_date',
                    'description' => esc_html__('Enter date in format dd/mm/yyyy.', 'mkdf-core')
                ),
                array(
                    'type' => 'attach_image',
                    'class' => '',
                    'heading' => esc_html__('Content Image', 'mkdf-core'),
                    'param_name' => 'content_image',
                    'value' => '',
                    'description' => ''
                )
            )
        ));
    }

    /**
     * Renders HTML for product list shortcode
     *
     * @param array $atts
     * @param null $content
     *
     * @return string
     */
    public function render($atts, $content = null) {
        $default_atts = array(
            'timeline_label' => '',
            'timeline_date' => '',
            'content_image' => '',
        );

        $params = shortcode_atts($default_atts, $atts);
        extract($params);
        $html = "<li class='mkdf-hti-content' data-date='" . esc_attr($timeline_date) . "'>";
        $html .= "<div class='mkdf-hti-content-inner'>";
        $html .= "<div class='mkdf-hti-content-inner-shadow'>";
        $html .= "<div class='mkdf-hti-content-image'>";
        $html .= wp_get_attachment_image($content_image, 'full');
        $html .= "</div>";
        $html .= "<div class='mkdf-hti-content-value'>";
        $html .= do_shortcode($content);
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</li>";

        return $html;
    }
}