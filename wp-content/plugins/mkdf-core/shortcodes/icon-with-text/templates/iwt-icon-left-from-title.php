<div <?php mediclinic_mikado_class_attribute($holder_classes); ?>>
	<div class="mkdf-iwt-content" <?php mediclinic_mikado_inline_style($content_styles); ?>>
		<?php if(!empty($title)) { ?>
			<<?php echo esc_attr($title_tag); ?> class="mkdf-iwt-title" <?php mediclinic_mikado_inline_style($title_styles); ?>>
				<?php if(!empty($link)) : ?>
					<a itemprop="url" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
				<?php endif; ?>
					<span class="mkdf-iwt-icon" <?php mediclinic_mikado_inline_style($icon_holder_styles); ?>>
						<?php if(!empty($custom_icon)) : ?>
                            <img src="<?php echo wp_get_attachment_image_url($custom_icon, 'full'); ?>" alt="<?php echo esc_attr('Custom icon', 'mkdf-core') ?>" <?php mediclinic_mikado_inline_style($custom_icon_styles); ?>/>
						<?php else: ?>
							<?php echo mkdf_core_get_shortcode_module_template_part('templates/icon', 'icon-with-text', '', array('icon_parameters' => $icon_parameters)); ?>
						<?php endif; ?>
					</span>
					<span class="mkdf-iwt-title-text"><?php echo esc_html($title); ?></span>
				<?php if(!empty($link)) : ?>
					</a>
				<?php endif; ?>
			</<?php echo esc_attr($title_tag); ?>>
		<?php } ?>
		<?php if(!empty($text)) { ?>
			<p class="mkdf-iwt-text" <?php mediclinic_mikado_inline_style($text_styles); ?>><?php echo esc_html($text); ?></p>
		<?php } ?>
	</div>
</div>