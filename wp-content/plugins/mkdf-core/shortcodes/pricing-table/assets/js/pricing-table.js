(function($) {
    'use strict';

    var pricingTable = {};
    mkdf.modules.pricingTable = pricingTable;

    pricingTable.mkdfInitPricingTable = mkdfInitPricingTable;


    pricingTable.mkdfOnDocumentReady = mkdfOnDocumentReady;

    $(document).ready(mkdfOnDocumentReady);

    /*
     All functions to be called on $(document).ready() should be in this function
     */
    function mkdfOnDocumentReady() {
        mkdfInitPricingTable();
    }

    /**
     * Init Pricing Table badge animation
     */
    function mkdfInitPricingTable() {
        var pricingTables = $('.mkdf-pricing-tables');

        if(pricingTables.length) {
            pricingTables.each(function () {
                pricingTables.appear(function() {
                    pricingTables.find('.mkdf-active-text').addClass('active');
                },{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
            });
        }
    }

})(jQuery);
