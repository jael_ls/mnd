<?php if ($type == 'carousel'){?>
	<div class="mkdf-clients-carousel-holder <?php echo esc_attr($holder_classes); ?>">
		<div class="mkdf-cc-inner <?php echo esc_attr($holder_inner_classes); ?>" <?php echo mediclinic_mikado_get_inline_attrs($carousel_data); ?>>
			<?php echo do_shortcode($content); ?>
		</div>
	</div>
<?php } ?>
<?php if ($type == 'grid'){?>
	<div <?php mediclinic_mikado_class_attribute($holder_classes); ?>>
		<?php echo do_shortcode($content); ?>
	</div>
<?php } ?>