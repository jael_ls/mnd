<?php if($image != '') { ?>
	<div class="mkdf-client-holder <?php echo esc_attr($class); ?>">
		<div class="mkdf-client-holder-inner">
			<div class="mkdf-client-image-holder">
				<?php if($link != '') { ?>
				<a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
					<?php } else { ?>
					<span class="mkdf-client-image-nolink">
                <?php } ?>
						<span class="mkdf-client-image">
						<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_url($image['alt']); ?>"/>
					</span>
						<?php if(!empty($hover_image)) { ?>
							<span class="mkdf-client-hover-image">
							<img src="<?php echo esc_url($hover_image['url']); ?>" alt="<?php echo esc_url($hover_image['alt']); ?>"/>
						</span>
						<?php } ?>
						<?php if($link != '') { ?>
				</a>
				<?php } else { ?>
				</span>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>