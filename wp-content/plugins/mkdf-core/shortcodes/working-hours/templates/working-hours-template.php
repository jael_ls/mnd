<div class="mkd-working-hours-holder" <?php mediclinic_mikado_inline_style($wh_colors); ?>>
    <?php if($show_icon || $custom_icon) : ?>
        <div class="mkd-wh-icon-holder" <?php mediclinic_mikado_inline_style($wh_colors); ?>>
            <span class="mkd-wh-icon">
                    <?php if($show_icon) { ?>
                        <?php echo mediclinic_mikado_icon_collections()->renderIcon($icon, $icon_pack, array(
                            'icon_attributes' => array(
                                'class' => 'mkd-btn-icon-elem',
                                'style' => $icon_styles
                            )
                        )); ?>
                    <?php } else echo wp_get_attachment_image($custom_icon, 'full'); ?>
            </span>
        </div>
    <?php endif; ?>
    <div class="mkd-wh-holder-inner"  <?php mediclinic_mikado_inline_style($margin); ?>>
        <div class="mkd-wh-holder-items">
        <?php if(is_array($working_hours) && count($working_hours)) : ?>
            <?php if($text !== '') : ?>
                <div class="mkd-wh-text-holder" <?php mediclinic_mikado_inline_style($text_style); ?>>
                    <p><?php echo esc_html($text); ?></p>
                </div>
            <?php endif; ?>

            <?php if($title !== '') : ?>
                <div class="mkd-wh-title-holder">
                    <h3 class="mkd-wh-title" <?php mediclinic_mikado_inline_style($title_color); ?>><?php echo esc_html($title); ?></h3>
                </div>
            <?php endif; ?>
            
            <?php foreach($working_hours as $working_hour) : ?>
                <div class="mkd-wh-item clearfix">
					<span class="mkd-wh-day" <?php mediclinic_mikado_inline_style($date_styles); ?>>
						<?php echo esc_html($working_hour['label']); ?>
					</span>
					<span class="mkd-wh-hours" <?php mediclinic_mikado_inline_style($date_styles); ?>>
						<?php if(isset($working_hour['time']) && $working_hour['time'] !== '') : ?>
                            <span class="mkd-wh-from"><?php echo esc_html($working_hour['time']); ?></span>
                        <?php endif; ?>
					</span>
                </div>
            <?php endforeach; ?>
        </div>
        <?php else: ?>
            <p><?php esc_html_e('Working hours haven\'t been set', 'mkdf-core'); ?></p>
        <?php endif; ?>
    </div>
</div>