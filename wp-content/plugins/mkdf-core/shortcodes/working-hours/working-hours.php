<?php
namespace MikadoCore\CPT\Shortcodes\WorkingHours;

use MikadoCore\Lib;

class WorkingHours implements Lib\ShortcodeInterface {
    private $base;

    public function __construct() {
        $this->base = 'mkdf_working_hours';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name'                      => esc_html__('Mikado Working Hours', 'mkdf-core'),
            'base'                      => $this->base,
            'category'                  => esc_html__('by MIKADO', 'mkdf-core'),
            'icon'                      => 'icon-wpb-working-hours extended-custom-icon',
            'allowed_container_element' => 'vc_row',
            'params'                    => array_merge(
                array(
                    array(
                        'type'        => 'dropdown',
                        'admin_label' => true,
                        'heading'     => esc_html__('Use Custom Icon', 'mkdf-core'),
                        'param_name'  => 'use_custom_icon',
                        'value'       => array(
                            esc_html__('Yes', 'mkdf-core') => 'yes',
                            esc_html__('No', 'mkdf-core') => 'no',
                        ),
                        'always_save' => true,
                        'description' => ''
                    ),
                    array(
                        'type'        => 'attach_image',
                        'admin_label' => true,
                        'heading'     => esc_html__('Custom Icon', 'mkdf-core'),
                        'param_name'  => 'custom_icon',
                        'always_save' => true,
                        'dependency' => array('element' => 'use_custom_icon', 'value' => 'yes')
                    )
                ),
                mediclinic_mikado_icon_collections()->getVCParamsArray(array('element' => 'use_custom_icon', 'value' => 'no'), '', true),
                array(
                    array(
                        'type'        => 'colorpicker',
                        'admin_label' => true,
                        'heading'     => esc_html__('Icon Color', 'mkdf-core'),
                        'param_name'  => 'icon_color',
                        'dependency' => array('element' => 'use_custom_icon', 'value' => 'no')
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Icon Bottom Margin', 'mkdf-core'),
                        'param_name'  => 'icon_margin',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'admin_label' => true,
                        'heading'     => esc_html__('Background Color', 'mkdf-core'),
                        'param_name'  => 'bckg_color'
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'admin_label' => true,
                        'heading'     => esc_html__('Text Color', 'mkdf-core'),
                        'param_name'  => 'text_color',
                        'description' => ''
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Title', 'mkdf-core'),
                        'param_name'  => 'title',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Custom Size (px) for Title', 'mkdf-core'),
                        'param_name'  => 'custom_size_title',
                        'value'       => ''
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Font Weight for Title', 'mkdf-core'),
                        'param_name'  => 'font_weight_title',
                        'value'       => array_flip(mediclinic_mikado_get_font_weight_array(true)),
                        'admin_label' => true,
                        'save_always' => true
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Text Transform for Title', 'mkdf-core'),
                        'param_name'  => 'text_transform_title',
                        'value'       => array_flip(mediclinic_mikado_get_text_transform_array(true)),
                        'admin_label' => true,
                        'save_always' => true
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Text', 'mkdf-core'),
                        'param_name'  => 'text',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Custom Size (px) for Text', 'mkdf-core'),
                        'param_name'  => 'custom_size_text',
                        'value'       => ''
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Custom Size (px) for Day and Time', 'mkdf-core'),
                        'param_name'  => 'custom_size_day',
                        'value'       => ''
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Format', 'mkdf-core'),
                        'param_name'  => 'format',
                        'admin_label' => true,
                        'value'       => array(
                            esc_html__('Work Days + Weekend', 'mkdf-core')                     => '5_2',
                            esc_html__('Work Days + Saturday + Sunday', 'mkdf-core')           => '5_1_1',
                            esc_html__('Same Throught the Week', 'mkdf-core')                  => '7',
                            esc_html__('Mon + Tue + Wed + Thu + Fri + Weekend', 'mkdf-core')   => '1_1_1_1_1_2',
                            esc_html__('Mon + Tue + Wed + Thu + Fri + Sat + Sun', 'mkdf-core') => '1_1_1_1_1_1_1',
                        ),
                        'save_always' => true
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Monday To Sunday', 'mkdf-core'),
                        'param_name'  => 'monday_to_sunday',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('7'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Monday To Friday', 'mkdf-core'),
                        'param_name'  => 'monday_to_friday',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('5_2', '5_1_1'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Weekend', 'mkdf-core'),
                        'param_name'  => 'weekend',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('5_2', '1_1_1_1_1_2'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Monday', 'mkdf-core'),
                        'param_name'  => 'monday',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('1_1_1_1_1_2', '1_1_1_1_1_1_1'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Tuesday', 'mkdf-core'),
                        'param_name'  => 'tuesday',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('1_1_1_1_1_2', '1_1_1_1_1_1_1'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Wednesday', 'mkdf-core'),
                        'param_name'  => 'wednesday',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('1_1_1_1_1_2', '1_1_1_1_1_1_1'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Thursday', 'mkdf-core'),
                        'param_name'  => 'thursday',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('1_1_1_1_1_2', '1_1_1_1_1_1_1'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Friday', 'mkdf-core'),
                        'param_name'  => 'friday',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('1_1_1_1_1_2', '1_1_1_1_1_1_1'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Saturday', 'mkdf-core'),
                        'param_name'  => 'saturday',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('5_1_1', '1_1_1_1_1_1_1'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Sunday', 'mkdf-core'),
                        'param_name'  => 'sunday',
                        'admin_label' => true,
                        'value'       => '',
                        'save_always' => true,
                        'group'       => esc_html__('Settings', 'mkdf-core'),
                        'dependency'  => array('element' => 'format', 'value' => array('5_1_1', '1_1_1_1_1_1_1'))
                    )
                )
            )
        ));
    }

    public function render($atts, $content = null) {
        $default_atts = array(
            'use_custom_icon'      => 'yes',
            'custom_icon'          => '',
            'bckg_color'           => '',
            'icon_color'           => '',
            'icon_margin'           => '',
            'text_color'           => '',
            'title'                => '',
            'custom_size_title'    => '',
            'font_weight_title'    => '',
            'text_transform_title' => '',
            'text'                 => '',
            'custom_size_text'     => '',
            'custom_size_day'      => '',
            'format'               => '',
            'monday_to_friday'     => '',
            'weekend'              => '',
            'monday'               => '',
            'tuesday'              => '',
            'wednesday'            => '',
            'thursday'             => '',
            'friday'               => '',
            'saturday'             => '',
            'sunday'               => '',
            'monday_to_sunday'     => ''
        );

        $default_atts = array_merge($default_atts, mediclinic_mikado_icon_collections()->getShortcodeParams());
        $params       = shortcode_atts($default_atts, $atts);

        $iconPackName   = mediclinic_mikado_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);
        $params['icon'] = $iconPackName ? $params[$iconPackName] : '';

        $params['working_hours'] = $this->getWorkingHours($params);
        $params['wh_colors']     = $this->getBckgStyles($params);
        $params['margin']       = $this->getMarginStyles($params);
        $params['icon_styles']    = $this->getIconStyles($params);
        $params['title_color']   = $this->getTitleStyles($params);
        $params['text_style']     = $this->getTextStyles($params);
        $params['date_styles']     = $this->getDayStyles($params);
        $params['show_icon']     = !empty($params['icon']);

        $html = mkdf_core_get_shortcode_module_template_part('templates/working-hours-template', 'working-hours', '', $params);

        return $html;

    }

    private function getWorkingHours($params) {
        $workingHours = array();

        if(!empty($params['format']) && $params['format'] === '5_2') {
            if(!empty($params['monday_to_friday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Monday - Friday', 'mkdf-core'),
                    'time'  => $params['monday_to_friday']
                );
            }

            if(!empty($params['weekend'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Saturday - Sunday', 'mkdf-core'),
                    'time'  => $params['weekend']
                );
            }
        } else if(!empty($params['format']) && $params['format'] === '1_1_1_1_1_1_1') {
            if(!empty($params['monday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Monday', 'mkdf-core'),
                    'time'  => $params['monday']
                );
            }

            if(!empty($params['tuesday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Tuesday', 'mkdf-core'),
                    'time'  => $params['tuesday']
                );
            }

            if(!empty($params['wednesday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Wednesday', 'mkdf-core'),
                    'time'  => $params['wednesday']
                );
            }

            if(!empty($params['thursday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Thursday', 'mkdf-core'),
                    'time'  => $params['thursday']
                );
            }

            if(!empty($params['friday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Friday', 'mkdf-core'),
                    'time'  => $params['friday']
                );
            }

            if(!empty($params['saturday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Saturday', 'mkdf-core'),
                    'time'  => $params['saturday']
                );
            }

            if(!empty($params['sunday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Sunday', 'mkdf-core'),
                    'time'  => $params['sunday']
                );
            }
        } else if(!empty($params['format']) && $params['format'] === '1_1_1_1_1_2') {
            if(!empty($params['monday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Monday', 'mkdf-core'),
                    'time'  => $params['monday']
                );
            }

            if(!empty($params['tuesday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Tuesday', 'mkdf-core'),
                    'time'  => $params['tuesday']
                );
            }

            if(!empty($params['wednesday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Wednesday', 'mkdf-core'),
                    'time'  => $params['wednesday']
                );
            }

            if(!empty($params['thursday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Thursday', 'mkdf-core'),
                    'time'  => $params['thursday']
                );
            }

            if(!empty($params['friday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Friday', 'mkdf-core'),
                    'time'  => $params['friday']
                );
            }

            if(!empty($params['weekend'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Saturday - Sunday', 'mkdf-core'),
                    'time'  => $params['weekend']
                );
            }
        } else if(!empty($params['format']) && $params['format'] === '5_1_1') {
            if(!empty($params['monday_to_friday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Monday - Friday', 'mkdf-core'),
                    'time'  => $params['monday_to_friday']
                );
            }

            if(!empty($params['saturday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Saturday', 'mkdf-core'),
                    'time'  => $params['saturday']
                );
            }

            if(!empty($params['sunday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Sunday', 'mkdf-core'),
                    'time'  => $params['sunday']
                );
            }
        } else if(!empty($params['format']) && $params['format'] === '7') {
            if(!empty($params['monday_to_sunday'])) {
                $workingHours[] = array(
                    'label' => esc_html__('Monday - Sunday', 'mkdf-core'),
                    'time'  => $params['monday_to_sunday']
                );
            }
        }

        return $workingHours;
    }

    private function getBckgStyles($params) {
        $styles = array();

        if($params['bckg_color'] !== '') {
            $styles[] = 'background-color: '.$params['bckg_color'];
        }

        if($params['text_color'] !== '') {
            $styles[] = 'color: '.$params['text_color'];
        }

        return $styles;
    }

    private function getMarginStyles($params) {
        $styles = '';

        if($params['icon_margin'] !== '') {
            $styles = 'margin-top:'.mediclinic_mikado_filter_px($params['icon_margin']).'px;';
        }

        return $styles;
    }

    private function getIconStyles($params) {
        $styles = '';
        $styles .= 'font-size:45px;';

        if($params['icon_color'] !== '') {
            $styles .= 'color:'.$params['icon_color'].';';
        }

        return $styles;
    }

    private function getTitleStyles($params) {
        $styles = array();

        if($params['text_color'] !== '') {
            $styles[] = 'color: '.$params['text_color'];
        }

        if(!empty($params['custom_size_title'])) {
            $styles[] = 'font-size:'.mediclinic_mikado_filter_px($params['custom_size_title']).'px';
        }

        if(!empty($params['font_weight_title'])) {
            $styles[] = 'font-weight: '.$params['font_weight_title'];
        }

        if(!empty($params['text_transform_title'])) {
            $styles[] = 'text-transform: '.$params['text_transform_title'];
        }

        return $styles;
    }

    private function getTextStyles($params) {
        $styles = array();

        if(!empty($params['custom_size_text'])) {
            $styles[] = 'font-size:'.mediclinic_mikado_filter_px($params['custom_size_text']).'px';
        }

        if(!empty($params['text_color'])) {
            $styles[] = 'color:'.$params['text_color'];
        }

        return $styles;
    }

    private function getDayStyles($params) {
        $styles = array();

        if(!empty($params['custom_size_day'])) {
            $styles[] = 'font-size:'.mediclinic_mikado_filter_px($params['custom_size_day']).'px';
        }
        if(!empty($params['text_color'])) {
            $styles[] = 'color:'.$params['text_color'];
        }

        return $styles;
    }
}
