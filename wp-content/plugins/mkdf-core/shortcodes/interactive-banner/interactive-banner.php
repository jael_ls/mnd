<?php

namespace MikadoCore\CPT\Shortcodes\InteractiveBanner;

use MikadoCore\Lib;

class InteractiveBanner implements Lib\ShortcodeInterface
{

    private $base;

    function __construct()
    {
        $this->base = 'mkdf_interactive_banner';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    public function vcMap()
    {
        if (function_exists('vc_map')) {
            vc_map(
                array(
                    'name' => esc_html__('Mikado Interactive Banner', 'mkdf-core'),
                    'base' => $this->base,
                    'category' => esc_html__( 'by MIKADO', 'mkdf-core' ),
                    'icon' => 'icon-wpb-interactive-banner extended-custom-icon',
                    'params' => array_merge(
                        array (
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Type', 'mkdf-core'),
                                'param_name' => 'type',
                                'value' => array(
                                    esc_html__('Standard', 'mkdf-core') => 'standard',
                                    esc_html__('Simple', 'mkdf-core') => 'simple',
                                    esc_html__('Info on hover', 'mkdf-core') => 'info-on-hover'
                                ),
                                'save_always' => true,
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Skin', 'mkdf-core'),
                                'param_name' => 'theme',
                                'value' => array(
                                    '' => '',
                                    esc_html__('Light', 'mkdf-core') => 'light',
                                    esc_html__('Dark', 'mkdf-core') => 'dark'
                                )
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Enable Icon', 'mkdf-core'),
                                'param_name' => 'enable_icons',
                                'value' => array(
                                    esc_html__('No', 'mkdf-core') => 'no',
                                    esc_html__('Yes', 'mkdf-core') => 'yes'
                                ),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            )
                        ),
                        \MediclinicMikadoIconCollections::get_instance()->getVCParamsArray(array('element' => 'enable_icons', 'value' => array('', 'yes'))),
                        array(
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__('Icon Color', 'mkdf-core'),
                                'param_name' => 'icon_color',
                                'description' => '',
                                'dependency' => array('element' => 'enable_icons', 'value' => array('', 'yes'))
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__('Padding', 'mkdf-core'),
                                'param_name' => 'padding',
                                'value' => '',
                                'description' => esc_html__('Please insert padding in format 0px 10px 0px 10px', 'mkdf-core')
                            ),
                            array(
                                'type' => 'attach_image',
                                'heading' => esc_html__('Background Image', 'mkdf-core'),
                                'param_name' => 'background_image'
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__('Background Color', 'mkdf-core'),
                                'param_name' => 'background_color'
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__('Background Hover Color', 'mkdf-core'),
                                'param_name' => 'background_hover_color'
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__('Title', 'mkdf-core'),
                                'param_name' => 'title',
                                'value' => '',
                                'admin_label' => true
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Title Tag', 'mkdf-core'),
                                'param_name' => 'title_tag',
                                'value' => array(
                                    '' => '',
                                    esc_html__('h2', 'mkdf-core') => 'h2',
                                    esc_html__('h3', 'mkdf-core') => 'h3',
                                    esc_html__('h4', 'mkdf-core') => 'h4',
                                    esc_html__('h5', 'mkdf-core') => 'h5',
                                    esc_html__('h6', 'mkdf-core') => 'h6',
                                ),
                                'dependency' => array('element' => 'title', 'not_empty' => true)
                            ),
                            array(
                                'type' => 'textarea',
                                'heading' => esc_html__('Text', 'mkdf-core'),
                                'param_name' => 'text',
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Enable Separator', 'mkdf-core'),
                                'param_name' => 'enable_separator',
                                'value' => array(
                                    '' => '',
                                    esc_html__('Yes', 'mkdf-core') => 'yes',
                                    esc_html__('No', 'mkdf-core') => 'no',
                                ),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__('Link', 'mkdf-core'),
                                'param_name' => 'link',
                                'value' => '',
                                'admin_label' => true
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__('Link Text', 'mkdf-core'),
                                'param_name' => 'link_text',
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Target', 'mkdf-core'),
                                'param_name' => 'mkdf-core',
                                'value' => array(
                                    '' => '',
                                    esc_html__('Self', 'mkdf-core') => '_self',
                                    esc_html__('Blank', 'mkdf-core') => '_blank'
                                )
                            ),
                            array(
                                'type' => 'dropdown',
                                'admin_label' => true,
                                'heading' => esc_html__('Item Alignment', 'mkdf-core'),
                                'param_name' => 'item_alignment',
                                'value' => array(
                                    esc_html__('Center', 'mkdf-core') => 'center',
                                    esc_html__('Left', 'mkdf-core') => 'left',
                                    esc_html__('Right', 'mkdf-core') => 'right'
                                ),
                                'save_always' => true
                            ),
                            array(
                                'type'        => 'dropdown',
                                'param_name'  => 'solid_skin',
                                'heading'     => esc_html__( 'Skin', 'mkdf-core' ),
                                'value'       => array(
                                    esc_html__( 'Dark', 'mkdf-core' )   => 'dark',
                                    esc_html__( 'Light', 'mkdf-core' ) => 'light'
                                ),
                                'admin_label' => true,
                                'save_always' => true,
                                'group'      => esc_html__( 'Button Options', 'mkdf-core' )
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__('Font Size', 'mkdf-core'),
                                'param_name' => 'button_font_size',
                                'group' => esc_html__('Button Options', 'mkdf-core'),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__('Button Left/Right padding', 'mkdf-core'),
                                'param_name' => 'button_padding',
                                'group' => esc_html__('Button Options', 'mkdf-core'),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__('Color', 'mkdf-core'),
                                'param_name' => 'button_color',
                                'group' => esc_html__('Button Options', 'mkdf-core'),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__('Hover Color', 'mkdf-core'),
                                'param_name' => 'button_hover_color',
                                'group' => esc_html__('Button Options', 'mkdf-core'),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__('Background Color', 'mkdf-core'),
                                'param_name' => 'button_background_color',
                                'group' => esc_html__('Button Options', 'mkdf-core'),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__('Hover Background Color', 'mkdf-core'),
                                'param_name' => 'button_hover_background_color',
                                'group' => esc_html__('Button Options', 'mkdf-core'),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__('Border Color', 'mkdf-core'),
                                'param_name' => 'button_border_color',
                                'group' => esc_html__('Button Options', 'mkdf-core'),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__('Hover Border Color', 'mkdf-core'),
                                'param_name' => 'button_hover_border_color',
                                'group' => esc_html__('Button Options', 'mkdf-core'),
                                'dependency' => array('element' => 'type', 'value' => array('standard', 'info-on-hover'))
                            ),
                        )
                    )
                )
            );
        }
    }

    public function render($atts, $content = null)
    {

        $args = array(
            'enable_icons' => 'no',
            'theme' => 'light',
            'type' => '',
            'icon_color' => '',
            'padding' => '',
            'background_image' => '',
            'background_color' => '',
            'background_hover_color' => '',
            'title' => '',
            'title_tag' => 'h3',
            'text' => '',
            'enable_separator' => 'no',
            'link' => '',
            'link_text' => 'Read More',
            'target' => '_self',
            'item_alignment' => 'center',
            'solid_skin'  => '',
            'button_color' => '',
            'button_hover_color' => '',
            'button_background_color' => '',
            'button_hover_background_color' => '',
            'button_border_color' => '',
            'button_hover_border_color' => '',
            'button_font_size' => '',
            'button_padding' => '',
        );

        $args = array_merge($args, mediclinic_mikado_icon_collections()->getShortcodeParams());

        $params = shortcode_atts($args, $atts);

        extract($params);
        $iconPackName = mediclinic_mikado_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);

        $html = '';

        $params['classes'] = $this->generateInteractiveBannerClasses($params);
        $params['style'] = $this->generateInteractiveBannerStyles($params);
        $params['hover-style'] = $this->generateInteractiveBannerOverlay($params);
        $params['initial-style'] = $this->generateInteractiveBannerInitial($params);
        $params['button_data'] = $this->getButtonParameters($params);
        $params['button_classes'] = $this->getButtonClasses($params);

        $html .= '<div class="' . $params['classes'] . '" style="' . $params['style'] . '">';
        $html .= '<a itemprop="url" class="mkdf-interactive-banner-cover-link" href="' . $params['link'] . '" target="' . $params['target'] . '"></a>';
        $html .= '<div class="mkdf-interactive-banner-initial" style="' . $params['initial-style'] . '"></div>';
        $html .= '<div class="mkdf-interactive-banner-overlay" style="' . $params['hover-style'] . '"></div>';
        if ($enable_icons == 'yes') {
            $params['icon'] = $params[$iconPackName];
            $params['icon_style'] = $this->generateIconStyles($params);
            $html .= mkdf_core_get_shortcode_module_template_part('templates/icon', 'interactive-banner', '', $params);
        }
        $html .= mkdf_core_get_shortcode_module_template_part('templates/interactive-banner-' . $params['type'], 'interactive-banner', '', $params);
        $html .= '</div>';

        return $html;
    }


    /**
     * Generates icon styles array
     *
     * @param $params
     *
     * @return string
     */
    private function generateIconStyles($params)
    {
        $iconStyles = array();

        if (!empty($params['icon_color'])) {
            $iconStyles[] = 'color: ' . $params['icon_color'];
        }

        return implode(';', $iconStyles);
    }

    /**
     * Generates banner classes
     *
     * @param $params
     *
     * @return string
     */

    private function generateInteractiveBannerClasses($params)
    {
        $interactive_banner_classes = array();
        $interactive_banner_classes[] = 'mkdf-interactive-banner-holder';

        if (!empty($params['item_alignment'])) {
            switch ($params['item_alignment']) {
                case 'left':
                    $interactive_banner_classes[] = 'mkdf-interactive-banner-align-left';
                    break;
                case 'center':
                    $interactive_banner_classes[] = 'mkdf-interactive-banner-align-center';
                    break;
                case 'right':
                    $interactive_banner_classes[] = 'mkdf-interactive-banner-align-right';
                    break;
                default:
                    break;
            }
        }

        if (!empty($params['theme'])) {
            switch ($params['theme']) {
                case 'light':
                    $interactive_banner_classes[] = 'mkdf-interactive-banner-light-theme';
                    break;
                case 'dark':
                    $interactive_banner_classes[] = 'mkdf-interactive-banner-dark-theme';
                    break;
                default:
                    break;
            }
        }

        if (!empty($params['type'])) {
            switch ($params['type']) {
                case 'standard':
                    $interactive_banner_classes[] = 'mkdf-interactive-banner-standard';
                    break;
                case 'simple':
                    $interactive_banner_classes[] = 'mkdf-interactive-banner-simple';
                    break;
                case 'info-on-hover':
                    $interactive_banner_classes[] = 'mkdf-interactive-banner-info-on-hover';
                    break;
                default:
                    break;
            }
        }

        return implode(' ', $interactive_banner_classes);
    }

    private function generateInteractiveBannerStyles($params)
    {
        $interactive_banner_styles = array();

        if (!empty($params['background_image'])) {
            $interactive_banner_styles[] = 'background-image: url(' . wp_get_attachment_url($params['background_image']) . ')';
        }

        if (!empty($params['padding'])) {
            $interactive_banner_styles[] = 'padding: ' . $params['padding'];
        }

        return implode(';', $interactive_banner_styles);
    }

    private function generateInteractiveBannerInitial($params)
    {
        $interactive_banner_initial_styles = array();

        if (!empty($params['background_color'])) {
            $interactive_banner_initial_styles[] = 'background-color: ' . $params['background_color'];
        }

        return implode(';', $interactive_banner_initial_styles);
    }

    private function generateInteractiveBannerOverlay($params)
    {
        $interactive_banner_overlay_styles = array();

        if (!empty($params['background_hover_color'])) {
            $interactive_banner_overlay_styles[] = 'background-color: ' . $params['background_hover_color'];
        }

        return implode(';', $interactive_banner_overlay_styles);
    }

    private function getButtonParameters($params)
    {
        $button_params_array = array();

        $buttonClasses = array(
            'mkdf-btn',
        );

        if($params['solid_skin'] != '') {
            $button_params_array['solid_skin'] = $params['solid_skin'];
        }

        if (!empty($params['link'])) {
            $button_params_array['link'] = $params['link'];
        }

        if (!empty($params['button_target'])) {
            $button_params_array['target'] = $params['button_target'];
        }

        if (!empty($params['link_text'])) {
            $button_params_array['text'] = $params['link_text'];
        }

        if (!empty($params['button_background_color'])) {
            $button_params_array['background_color'] = $params['button_background_color'];
        }

        if (!empty($params['button_border_color'])) {
            $button_params_array['border_color'] = $params['button_border_color'];
        }

        if (!empty($params['button_color'])) {
            $button_params_array['color'] = $params['button_color'];
        }

        if (!empty($params['button_hover_color'])) {
            $button_params_array['hover_color'] = $params['button_hover_color'];
        }

        if (!empty($params['button_hover_background_color'])) {
            $button_params_array['hover_background_color'] = $params['button_hover_background_color'];
        }

        if (!empty($params['button_hover_border_color'])) {
            $button_params_array['hover_border_color'] = $params['button_hover_border_color'];
        }

        if (!empty($params['button_font_size'])) {
            $button_params_array['font_size'] = $params['button_font_size'];
        }

        if (!empty($params['button_padding'])) {
            $button_params_array['padding'] = $params['button_padding'];
        }

        return $button_params_array;
    }

    /**
     * Returns array of HTML classes for button
     *
     * @param $params
     *
     * @return array
     */
    private function getButtonClasses($params)
    {
        $buttonClasses = array(
            'mkdf-btn'
        );

        return $buttonClasses;
    }

}