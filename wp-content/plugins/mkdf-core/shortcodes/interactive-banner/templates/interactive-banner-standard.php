<div class="mkdf-interactive-banner-inner-wrapper">
    <div class="mkdf-interactive-banner-content-holder" >
        <?php if($title !== '' ) : ?>
            <div class="mkdf-interactive-banner-title-holder">
                <<?php echo esc_attr($title_tag); ?>>
                    <?php echo esc_html($title); ?>
                </<?php echo esc_attr($title_tag); ?>>
            </div>
        <?php endif; ?>
        <?php if($enable_separator == 'yes' ) : ?>
            <?php echo mediclinic_mikado_execute_shortcode('mkdf_separator', array("with_icon" => "yes", "icon_pack" => "font_awesome", "fa_icon"=> "fa-university")); ?>
        <?php endif; ?>
        <div class="mkdf-interactive-banner-text-holder">
            <p><?php echo esc_html($text); ?></p>
            <?php if(!empty($link) && !empty($link_text)) :?>
                <?php $button_data = array_merge($button_data, array('ion_icon' => 'ion-chevron-right', 'icon_pack' => 'ion_icons')); ?>
                <?php echo mediclinic_mikado_get_button_html($button_data); ?>
            <?php endif; ?>
        </div>
    </div>
</div>