<?php
$icon_html = mediclinic_mikado_icon_collections()->renderIcon($icon, $icon_pack, $params);

?>

<div class="mkdf-interactive-banner-icon" <?php mediclinic_mikado_inline_style($icon_style); ?> data-color="<?php if($icon_color){ echo esc_attr($icon_color);} ?>">
    <?php print $icon_html; ?>
</div>
