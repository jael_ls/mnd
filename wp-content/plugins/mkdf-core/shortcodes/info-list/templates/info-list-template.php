<div class="mkdf-info-list">
    <div class="mkdf-info-list-inner">
        <?php if($title != '') : ?>
            <h3 class="mkdf-info-list-title"><?php echo esc_html($title); ?></h3>
        <?php endif; ?>

        <div class="mkdf-info-list-items">
            <?php echo do_shortcode($content); ?>
        </div>

        <?php
        if($link_text != '' && $link != '') {
            echo mediclinic_mikado_get_button_html(array(
                'size'               => 'medium',
                'type'               => 'solid',
                'text'               => $link_text,
                'link'               => $link,
                'target'             => $target,
                'icon_pack'          => 'ion_icons',
                'ion_icon'            => 'ion-chevron-right',
                'custom_class'       => 'mkdf-info-list-button'
            ));
        }
        ?>
    </div>
</div>