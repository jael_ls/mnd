<div class="mkdf-info-list-item">
	<?php if (!empty($link)) : ?>
	<a class="mkdf-ili-link" href="<?php echo esc_url($link);?>" target="<?php echo esc_attr($target); ?>">
	<?php endif; ?>
		<div class="mkdf-info-list-item-inner">
			<span class="mkdf-ili-left"><?php if ($title != '') echo '<span class="mkdf-ili-title">' . esc_html($title) . '</span>'; ?><?php if ($subtitle != '') echo '<span class="mkdf-ili-subtitle">(' . esc_html($subtitle) . ')</span>'; ?></span>
			<span class="mkdf-ili-right"><?php echo esc_html($info); ?></span>
		</div>
	<?php if (!empty($link)) : ?>
	</a>
	<?php endif; ?>
</div>