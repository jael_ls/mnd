<?php
namespace MikadoCore\CPT\Shortcodes\InfoList;

use MikadoCore\Lib;

/**
 * class Accordions
 */
class InfoList implements Lib\ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	function __construct() {
		$this->base = 'mkdf_info_list';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {

		vc_map(array(
			'name'                    => esc_html__('Mikado Info List', 'mkdf-core'),
			'base'                    => $this->base,
			'as_parent'               => array('only' => 'mkdf_info_list_item'),
			'content_element'         => true,
			'category'                => esc_html__('by MIKADO', 'mkdf-core'),
			'icon'                    => 'icon-wpb-info-list extended-custom-icon',
			'show_settings_on_create' => true,
			'js_view'                 => 'VcColumnView',
			'params'                  => array(
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__('Title', 'mkdf-core'),
					'param_name'  => 'title',
					'admin_label' => true,
					'value'       => ''
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__('Button Text', 'mkdf-core'),
					'param_name'  => 'link_text',
					'admin_label' => true,
					'description' => esc_html__('Enter text for the button below the list items.', 'mkdf-core')
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__('Button Link', 'mkdf-core'),
					'param_name'  => 'link',
					'admin_label' => true,
					'dependency'  => array('element' => 'link_text', 'not_empty' => true)
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__('Button Target', 'mkdf-core'),
					'param_name'  => 'target',
					'value'       => array(
						esc_html__('Self', 'mkdf-core')  => '_self',
						esc_html__('Blank', 'mkdf-core') => '_blank'
					),
					'save_always' => true,
					'admin_label' => true,
					'dependency'  => array('element' => 'link_text', 'not_empty' => true)
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$default_atts = array(
			'title' => '',
			'link_text' => '',
			'link' => '',
			'target' => ''
		);

		$params       = shortcode_atts($default_atts, $atts);
		extract($params);

		$params['content']   = $content;

		$output = '';

		$output .= mkdf_core_get_shortcode_module_template_part('templates/info-list-template', 'info-list', '', $params);

		return $output;
	}
}
