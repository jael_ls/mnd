<?php

namespace MikadoCore\CPT\Shortcodes\InfoListItem;

use MikadoCore\Lib;

/**
 * class Accordions
 */
class InfoListItem implements Lib\ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	function __construct() {
		$this->base = 'mkdf_info_list_item';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map(array(
				"name"                    => esc_html__('Info List Item', 'mkdf-core'),
				"base"                    => $this->base,
				"as_child"                => array('only' => 'mkdf_info_list'),
				"category"                => esc_html__('by MIKADO', 'mkdf-core'),
				"icon"                    => "icon-wpb-info-list-item extended-custom-icon",
				"show_settings_on_create" => true,
				'params'                  => array_merge(
					array(
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__('Title', 'mkdf-core'),
							'param_name'  => 'title',
							'admin_label' => true,
							'value'       => '',
							'description' => esc_html__('Enter item title.', 'mkdf-core')
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__('Subtitle', 'mkdf-core'),
							'param_name'  => 'subtitle',
							'admin_label' => true,
							'value'       => '',
							'description' => esc_html__('Enter text to be displayed in brackets.', 'mkdf-core')
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__('Info Text', 'mkdf-core'),
							'param_name'  => 'info',
							'admin_label' => true,
							'description' => esc_html__('Enter text to be displayed as item info.', 'mkdf-core')
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__('Link', 'mkdf-core'),
							'param_name'  => 'link',
							'admin_label' => true
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__('Link Target', 'mkdf-core'),
							'param_name'  => 'target',
							'value'       => array(
								esc_html__('Self', 'mkdf-core')  => '_self',
								esc_html__('Blank', 'mkdf-core') => '_blank'
							),
							'save_always' => true,
							'admin_label' => true
						)
					)
				)
			));
		}
	}


	public function render($atts, $content = null) {

		$default_atts = (array(
			'title' => '',
			'subtitle' => '',
			'info' => '',
			'link' => '',
			'target' => ''
		));

		$params       = shortcode_atts($default_atts, $atts);

		extract($params);

		$output = '';

		$output .= mkdf_core_get_shortcode_module_template_part('templates/info-list-item-template', 'info-list', '', $params);

		return $output;

	}

}