<?php
/*
Plugin Name: Mikado Instagram Feed
Description: Plugin that adds Instagram feed functionality to our theme
Author: Mikado Themes
Version: 2.0
*/
define('MIKADOF_INSTAGRAM_FEED_VERSION', '2.0');

include_once 'load.php';